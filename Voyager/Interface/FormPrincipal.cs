﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Voyager.Interface;
using Voyager.Interface.Cadastro;
using Voyager.Interface.Tabela;
using Voyager.Interface.Tabela.Fornecedor;

namespace Voyager.Interface
{
    public partial class FormPrincipal : Form
    {
        public FormPrincipal()
        {
            InitializeComponent();
            HabilitarMenu(false);
            FormConexao loFormConexao = new FormConexao();
            loFormConexao.MdiParent = this;
            loFormConexao.Show();
        }

        public void HabilitarMenu(bool abHabilitar)
        {
            menuStrip1.Enabled = abHabilitar;
        }
        private void FormPrincipal_Load(object sender, EventArgs e)
        {

        }

        private void cidadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormTabCidade newMDIChild = new FormTabCidade();
            newMDIChild.MdiParent = this;
            newMDIChild.Show();
        }

        private void estadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormTabEstado newMDIChild = new FormTabEstado();
            newMDIChild.MdiParent = this;
            newMDIChild.Show();
        }

        private void tipoDeFornecedorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormTipoFornecedor newMDIChild = new FormTipoFornecedor();
            newMDIChild.MdiParent = this;
            newMDIChild.Show();
        }

        private void fornecedorToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FormFornecedor newMDIChild = new FormFornecedor();
            newMDIChild.MdiParent = this;
            newMDIChild.Show();
        }

        private void clienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormCliente newMDIChild = new FormCliente();
            newMDIChild.MdiParent = this;
            newMDIChild.Show();
        }

        private void viagemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormViagem newMDIChild = new FormViagem();
            newMDIChild.MdiParent = this;
            newMDIChild.Show();
        }
    }
}
