﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Voyager.Interface.Cadastro
{
    public partial class FormCliente : Form
    {
        DataSet ioDataSetVoyager;
        SqlDataAdapter ioSqlDataAdapterCliente;
        BindingSource ioBindingSourceCliente;
        int ii_width_old = 0;
        int ii_heigth_old = 0;
        SqlConnection loConexao;

        public FormCliente()
        {
            InitializeComponent();
            loConexao = GerenciadorAplicacao.ConexaoBancoDados.DbConnectionStringBuilder;
            this.ArmazenarAlturaLarguraTela();
            this.dataGridViewCliente.AutoResizeColumns();
            this.dataGridViewCliente.AutoSizeColumnsMode =
                DataGridViewAutoSizeColumnsMode.AllCells;
            ExecutarConsultaCliente();
        }

        /// <summary>
        /// Eventos de clique dos botões do menu...
        /// </summary>

        private void toolStripSaveButton_Click(object sender, EventArgs e)
        {
            dataGridViewCliente.CommitEdit(DataGridViewDataErrorContexts.Commit);
            this.ioBindingSourceCliente.EndEdit();
            for (int i = 0; i < dataGridViewCliente.RowCount; i++)
            {
                DataGridViewRow gridrow = dataGridViewCliente.Rows[i];
                DataRowView rowview = (DataRowView)gridrow.DataBoundItem;
                DataRow row = rowview.Row;

                if (row.RowState.Equals(DataRowState.Added))
                {
                    dataGridViewCliente.Rows[i].Cells["ColumnIdEmpresa"].Value = 1;
                    dataGridViewCliente.Rows[i].Cells["ColumnNmUsuario"].Value = GerenciadorAplicacao.UsuarioAtual.Login;
                    dataGridViewCliente.Rows[i].Cells["ColumnDtAtualizacao"].Value = DateTime.Today;
                }
                else if (row.RowState.Equals(DataRowState.Modified))
                {
                    dataGridViewCliente.Rows[i].Cells["ColumnNmUsuario"].Value = GerenciadorAplicacao.UsuarioAtual.Login;
                    dataGridViewCliente.Rows[i].Cells["ColumnDtAtualizacao"].Value = DateTime.Today;
                }
            }

            try
            {
                if (this.ioSqlDataAdapterCliente.Update((DataTable)this.ioBindingSourceCliente.DataSource) >= 1)
                {
                    MessageBox.Show("Operação executada com sucesso!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.ToString());
            }
        }

        /// <summary>
        /// Metodos de execução no banco de dados, definição de scripts de conexão e parametros... 
        /// </summary>

        private void ExecutarConsultaCliente()
        {
            SqlCommand loSqlCommandSelect = ObterSqlCommandCliente();

            this.ioSqlDataAdapterCliente = new SqlDataAdapter(loSqlCommandSelect);
            SqlCommandBuilder loSqlCommandBuilderCliente = new SqlCommandBuilder(this.ioSqlDataAdapterCliente);
            this.ioDataSetVoyager = new DataSet();
            this.ioSqlDataAdapterCliente.Fill(this.ioDataSetVoyager);
            this.ioBindingSourceCliente = new BindingSource();
            this.ioBindingSourceCliente.DataSource = this.ioDataSetVoyager.Tables[0];
            this.bindingNavigatorVoyager.BindingSource = this.ioBindingSourceCliente;
            this.dataGridViewCliente.DataSource = this.ioBindingSourceCliente;
        }

        private SqlCommand ObterSqlCommandCliente()
        {
            SqlCommand loSqlCommand = new SqlCommand(
                "SELECT clt_id_cliente, " +
                "clt_nm_cliente, " +
                "clt_nu_telefone, " +
                "clt_nu_rg, " +
                "clt_nu_cpf, " +
                "clt_ds_endereco, " +
                "clt_dt_nascimento, " +
                "clt_id_empresa, " +
                "clt_nm_usuario, " +
                "clt_dt_atualizacao " +
                "FROM CLT_cliente " +
                "WHERE clt_id_empresa = @idEmpresa",loConexao);

            loSqlCommand.Parameters.Add(GerarParametroCliente("@idEmpresa", 1));

            return loSqlCommand;
        }

        private SqlParameter GerarParametroCliente(string nomeParametro, object value)
        {
            SqlParameter loSqlParameterCliente = new SqlParameter();
            loSqlParameterCliente.ParameterName = nomeParametro;
            loSqlParameterCliente.Value = value;
            return loSqlParameterCliente;
        }
        /// <summary>
        /// Metodos de redimensionamento da tela e persistência das informações ao adicionar uma nova linha....
        /// <summary>

        private void RedimensionarTela(object sender, EventArgs e)
        {
            if (WindowState != FormWindowState.Minimized)
            {
                Size tempSize = this.Size;
                tempSize.Height = this.dataGridViewCliente.Size.Height + this.Size.Height - ii_heigth_old;
                tempSize.Width = this.dataGridViewCliente.Size.Width + this.Size.Width - ii_width_old;
                ArmazenarAlturaLarguraTela();
                this.dataGridViewCliente.Size = tempSize;
            }
        }

        private void ArmazenarAlturaLarguraTela()
        {
            ii_width_old = this.Size.Width;
            ii_heigth_old = this.Size.Height;
        }

        private void dataGridViewCliente_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            dataGridViewCliente.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

        
    }
}
