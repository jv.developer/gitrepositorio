﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Voyager.Interface.Cadastro
{
    public partial class FormViagem : Form
    {
        DataSet ioDataSetVoyager;
        SqlDataAdapter ioSqlDataAdapterViagem;
        BindingSource ioBindingSourceViagem;
        int ii_width_old = 0;
        int ii_heigth_old = 0;
        SqlConnection loConexao;

        public FormViagem()
        {
            InitializeComponent();
            loConexao = GerenciadorAplicacao.ConexaoBancoDados.DbConnectionStringBuilder;
            this.ArmazenarAlturaLarguraTela();
            this.dataGridViewViagem.AutoResizeColumns();
            this.dataGridViewViagem.AutoSizeColumnsMode =
                DataGridViewAutoSizeColumnsMode.AllCells;
            DateTime data = DateTime.Today;
            this.dateTimePickerInicial.Value = new DateTime(data.Year, data.Month, 1);
            DefinirCbFiltroCidade();
            DefinirComboBoxCidade();
            DefinirComboBoxFornecedor();
            DefinirComboBoxCliente();
        }

        /// <summary>
        /// Eventos de clique dos botões do menu...
        /// </summary>

        private void toolStripBuscarButton_Click(object sender, EventArgs e)
        {
            ExecutarConsultaViagem();
        }

        /// <summary>
        /// Metodos de execução no banco de dados, definição de scripts de conexão e parametros... 
        /// </summary>

        private void ExecutarConsultaViagem()
        {
            SqlCommand loSqlCommandSelectViagem = ObterSqlCommandViagem();

            this.ioSqlDataAdapterViagem = new SqlDataAdapter(loSqlCommandSelectViagem);
            SqlCommandBuilder loSqlCommandBuilderViagem = new SqlCommandBuilder(this.ioSqlDataAdapterViagem);
            this.ioDataSetVoyager = new DataSet();
            this.ioSqlDataAdapterViagem.Fill(this.ioDataSetVoyager);
            this.ioBindingSourceViagem = new BindingSource();
            this.ioBindingSourceViagem.DataSource = this.ioDataSetVoyager.Tables[0];
            this.bindingNavigatorVoyager.BindingSource = this.ioBindingSourceViagem;
            this.dataGridViewViagem.DataSource = this.ioBindingSourceViagem;
        }

        private SqlCommand ObterSqlCommandViagem()
        {
            SqlCommand loSqlCommandViagem = new SqlCommand(
                "SELECT via_id_viagem, " +
                "via_id_fornecedor, " +
                "via_id_cidade, " +
                "via_dt_ida, " +
                "via_dt_volta, " +
                "via_vl_unitario, " +
                "via_id_empresa, " +
                "via_nm_usuario, " +
                "via_dt_atualizacao " +
                "FROM VIA_viagem " +
                "WHERE via_id_cidade = @idCidade or @idCidade = -1", loConexao);

            loSqlCommandViagem.Parameters.Add(GerarParametroDataGrid("@idCidade", Convert.ToInt32(this.comboBoxCidade.SelectedValue)));

            return loSqlCommandViagem;
        }

        private void ExecutarConsultaClienteViagem()
        {
            SqlCommand loSqlCommandClienteViagem = ObterSqlCommandClienteViagem();

            this.ioSqlDataAdapterViagem = new SqlDataAdapter(loSqlCommandClienteViagem);
            //teste...]
            //outro.......
        }

        private SqlCommand ObterSqlCommandClienteViagem()
        {
            SqlCommand loSqlCommandClienteViagem = new SqlCommand(
                "", loConexao);

            return loSqlCommandClienteViagem;
        }

        private SqlParameter GerarParametroDataGrid(string nomeParametro, object value)
        {
            SqlParameter loSqlParameterDataGrid = new SqlParameter();
            loSqlParameterDataGrid.ParameterName = nomeParametro;
            loSqlParameterDataGrid.Value = value;
            return loSqlParameterDataGrid;
        }

        /// <summary>
        /// Metodos de definição de ComboBox/Colunas do DataGridView...
        /// </summary>
        
        private void DefinirComboBoxFornecedor()
        {
            SqlCommand loSqlCommandCbColuna = new SqlCommand(
                "SELECT (frn_id_fornecedor) cd_dado, " +
                "(frn_nm_nome) ds_exibicao " +
                "FROM FRN_fornecedor", loConexao);

            SqlDataAdapter loSqlDataAdapterCbColuna = new SqlDataAdapter(loSqlCommandCbColuna);
            DataSet loDataSetCbColuna = new DataSet();
            loSqlDataAdapterCbColuna.Fill(loDataSetCbColuna);
            this.ColumnIdFornecedor.DisplayMember = "ds_exibicao";
            this.ColumnIdFornecedor.ValueMember = "cd_dado";
            this.ColumnIdFornecedor.DataSource = loDataSetCbColuna.Tables[0];
        }

        private void DefinirComboBoxCidade()
        {
            SqlCommand loSqlCommandCbColuna = new SqlCommand(
                "SELECT (cid_id_cidade) cd_dado, " +
                "(cid_nm_cidade) ds_exibicao " +
                "FROM CID_cidade", loConexao);

            SqlDataAdapter loSqlDataAdapterCbColuna = new SqlDataAdapter(loSqlCommandCbColuna);
            DataSet loDataSetCbColuna = new DataSet();
            loSqlDataAdapterCbColuna.Fill(loDataSetCbColuna);
            this.ColumnIdCidade.DisplayMember = "ds_exibicao";
            this.ColumnIdCidade.ValueMember = "cd_dado";
            this.ColumnIdCidade.DataSource = loDataSetCbColuna.Tables[0];
        }

        private void DefinirComboBoxCliente()
        {
            SqlCommand loSqlCommandCbColuna = new SqlCommand(
                "SELECT (clt_id_cliente) cd_dado, " +
                "(clt_nm_cliente) ds_exibicao " +
                "FROM CLT_cliente", loConexao);

            SqlDataAdapter loSqlDataAdapterCbColuna = new SqlDataAdapter(loSqlCommandCbColuna);
            DataSet loDataSetCbColuna = new DataSet();
            loSqlDataAdapterCbColuna.Fill(loDataSetCbColuna);
            this.ColumnIdCliente.DisplayMember = "ds_exibicao";
            this.ColumnIdCliente.ValueMember = "cd_dado";
            this.ColumnIdCliente.DataSource = loDataSetCbColuna.Tables[0];
        }

        /// <summary>
        /// Metodos de definição dos filtros de busca..
        /// </summary>
        
        private void DefinirCbFiltroCidade()
        {
            SqlCommand loSqlCommandCbFiltro = new SqlCommand(
                "SELECT (cid_id_cidade) cd_dado, " +
                "(cid_nm_cidade) ds_exibicao " +
                "FROM CID_cidade " +
                "UNION " +
                "SELECT -1, '[TODOS]' " +
                "UNION " +
                "SELECT -2, '[INDEFINIDO]' " +
                "ORDER BY ds_exibicao", loConexao);

            SqlDataAdapter loSqlDataAdapterCbFiltroPropriedade = new SqlDataAdapter(loSqlCommandCbFiltro);
            DataSet loDataSetCbFiltroPropriedade = new DataSet();
            loSqlDataAdapterCbFiltroPropriedade.Fill(loDataSetCbFiltroPropriedade);
            this.comboBoxCidade.DisplayMember = "ds_exibicao";
            this.comboBoxCidade.ValueMember = "cd_dado";
            this.comboBoxCidade.DataSource = loDataSetCbFiltroPropriedade.Tables[0];
        }

        /// <summary>
        /// Metodos de redimensionamento da tela e persistência das informações ao adicionar uma nova linha....
        /// <summary>

        private void RedimensionarTela(object sender, EventArgs e)
        {
            if (WindowState != FormWindowState.Minimized)
            {
                Size tempSize = this.Size;
                tempSize.Height = this.dataGridViewViagem.Size.Height + this.Size.Height - ii_heigth_old;
                tempSize.Width = this.dataGridViewViagem.Size.Width + this.Size.Width - ii_width_old;
                ArmazenarAlturaLarguraTela();
                this.dataGridViewViagem.Size = tempSize;
            }
        }

        private void ArmazenarAlturaLarguraTela()
        {
            ii_width_old = this.Size.Width;
            ii_heigth_old = this.Size.Height;
        }

        
    }
}
