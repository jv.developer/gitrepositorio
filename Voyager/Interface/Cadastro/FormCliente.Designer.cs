﻿namespace Voyager.Interface.Cadastro
{
    partial class FormCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCliente));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bindingNavigatorVoyager = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.toolStripBuscarButton = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSaveButton = new System.Windows.Forms.ToolStripButton();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridViewCliente = new System.Windows.Forms.DataGridView();
            this.ColumnId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnNmCliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnNuTelefone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnNuRg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnNuCpf = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDsEndereco = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDtNascimento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnIdEmpresa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnNmUsuario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDtAtualizacao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigatorVoyager)).BeginInit();
            this.bindingNavigatorVoyager.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCliente)).BeginInit();
            this.SuspendLayout();
            // 
            // bindingNavigatorVoyager
            // 
            this.bindingNavigatorVoyager.AddNewItem = this.bindingNavigatorAddNewItem;
            this.bindingNavigatorVoyager.CountItem = this.bindingNavigatorCountItem;
            this.bindingNavigatorVoyager.DeleteItem = this.bindingNavigatorDeleteItem;
            this.bindingNavigatorVoyager.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripBuscarButton,
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.toolStripSaveButton});
            this.bindingNavigatorVoyager.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigatorVoyager.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.bindingNavigatorVoyager.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.bindingNavigatorVoyager.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.bindingNavigatorVoyager.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.bindingNavigatorVoyager.Name = "bindingNavigatorVoyager";
            this.bindingNavigatorVoyager.PositionItem = this.bindingNavigatorPositionItem;
            this.bindingNavigatorVoyager.Size = new System.Drawing.Size(800, 25);
            this.bindingNavigatorVoyager.TabIndex = 4;
            this.bindingNavigatorVoyager.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Adicionar novo";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(37, 22);
            this.bindingNavigatorCountItem.Text = "de {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Número total de itens";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Excluir";
            // 
            // toolStripBuscarButton
            // 
            this.toolStripBuscarButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripBuscarButton.Image = ((System.Drawing.Image)(resources.GetObject("toolStripBuscarButton.Image")));
            this.toolStripBuscarButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripBuscarButton.Name = "toolStripBuscarButton";
            this.toolStripBuscarButton.Size = new System.Drawing.Size(23, 22);
            this.toolStripBuscarButton.Text = "toolStripButton1";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Mover primeiro";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Mover anterior";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Posição";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Posição atual";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Mover próximo";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Mover último";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSaveButton
            // 
            this.toolStripSaveButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripSaveButton.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSaveButton.Image")));
            this.toolStripSaveButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSaveButton.Name = "toolStripSaveButton";
            this.toolStripSaveButton.Size = new System.Drawing.Size(23, 22);
            this.toolStripSaveButton.Text = "toolStripButton1";
            this.toolStripSaveButton.Click += new System.EventHandler(this.toolStripSaveButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Teal;
            this.label2.Location = new System.Drawing.Point(22, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Registros";
            // 
            // groupBox2
            // 
            this.groupBox2.ForeColor = System.Drawing.Color.Teal;
            this.groupBox2.Location = new System.Drawing.Point(12, 28);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(775, 10);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            // 
            // dataGridViewCliente
            // 
            this.dataGridViewCliente.AllowUserToAddRows = false;
            this.dataGridViewCliente.AllowUserToResizeColumns = false;
            this.dataGridViewCliente.AllowUserToResizeRows = false;
            this.dataGridViewCliente.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.dataGridViewCliente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCliente.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnId,
            this.ColumnNmCliente,
            this.ColumnNuTelefone,
            this.ColumnNuRg,
            this.ColumnNuCpf,
            this.ColumnDsEndereco,
            this.ColumnDtNascimento,
            this.ColumnIdEmpresa,
            this.ColumnNmUsuario,
            this.ColumnDtAtualizacao});
            this.dataGridViewCliente.Location = new System.Drawing.Point(12, 47);
            this.dataGridViewCliente.Name = "dataGridViewCliente";
            this.dataGridViewCliente.RowHeadersVisible = false;
            this.dataGridViewCliente.Size = new System.Drawing.Size(776, 291);
            this.dataGridViewCliente.TabIndex = 13;
            this.dataGridViewCliente.CurrentCellDirtyStateChanged += new System.EventHandler(this.dataGridViewCliente_CurrentCellDirtyStateChanged);
            // 
            // ColumnId
            // 
            this.ColumnId.DataPropertyName = "clt_id_cliente";
            this.ColumnId.HeaderText = "ColumnId";
            this.ColumnId.Name = "ColumnId";
            this.ColumnId.Visible = false;
            // 
            // ColumnNmCliente
            // 
            this.ColumnNmCliente.DataPropertyName = "clt_nm_cliente";
            this.ColumnNmCliente.HeaderText = "Nome";
            this.ColumnNmCliente.Name = "ColumnNmCliente";
            // 
            // ColumnNuTelefone
            // 
            this.ColumnNuTelefone.DataPropertyName = "clt_nu_telefone";
            this.ColumnNuTelefone.HeaderText = "Telefone";
            this.ColumnNuTelefone.Name = "ColumnNuTelefone";
            // 
            // ColumnNuRg
            // 
            this.ColumnNuRg.DataPropertyName = "clt_nu_rg";
            this.ColumnNuRg.HeaderText = "RG";
            this.ColumnNuRg.Name = "ColumnNuRg";
            // 
            // ColumnNuCpf
            // 
            this.ColumnNuCpf.DataPropertyName = "clt_nu_cpf";
            this.ColumnNuCpf.HeaderText = "CPF";
            this.ColumnNuCpf.Name = "ColumnNuCpf";
            // 
            // ColumnDsEndereco
            // 
            this.ColumnDsEndereco.DataPropertyName = "clt_ds_endereco";
            this.ColumnDsEndereco.HeaderText = "Endereço";
            this.ColumnDsEndereco.Name = "ColumnDsEndereco";
            // 
            // ColumnDtNascimento
            // 
            this.ColumnDtNascimento.DataPropertyName = "clt_dt_nascimento";
            dataGridViewCellStyle1.Format = "d";
            dataGridViewCellStyle1.NullValue = null;
            this.ColumnDtNascimento.DefaultCellStyle = dataGridViewCellStyle1;
            this.ColumnDtNascimento.HeaderText = "Data de Nascimento";
            this.ColumnDtNascimento.Name = "ColumnDtNascimento";
            // 
            // ColumnIdEmpresa
            // 
            this.ColumnIdEmpresa.DataPropertyName = "clt_id_empresa";
            this.ColumnIdEmpresa.HeaderText = "ColumnIdEmpresa";
            this.ColumnIdEmpresa.Name = "ColumnIdEmpresa";
            this.ColumnIdEmpresa.Visible = false;
            // 
            // ColumnNmUsuario
            // 
            this.ColumnNmUsuario.DataPropertyName = "clt_nm_usuario";
            this.ColumnNmUsuario.HeaderText = "ColumnNmUsuario";
            this.ColumnNmUsuario.Name = "ColumnNmUsuario";
            this.ColumnNmUsuario.Visible = false;
            // 
            // ColumnDtAtualizacao
            // 
            this.ColumnDtAtualizacao.DataPropertyName = "clt_dt_atualizacao";
            this.ColumnDtAtualizacao.HeaderText = "ColumnDtAtualizacao";
            this.ColumnDtAtualizacao.Name = "ColumnDtAtualizacao";
            this.ColumnDtAtualizacao.Visible = false;
            // 
            // FormCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.dataGridViewCliente);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.bindingNavigatorVoyager);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormCliente";
            this.Text = "Cadastro - Cliente";
            this.Resize += new System.EventHandler(this.RedimensionarTela);
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigatorVoyager)).EndInit();
            this.bindingNavigatorVoyager.ResumeLayout(false);
            this.bindingNavigatorVoyager.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCliente)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingNavigator bindingNavigatorVoyager;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton toolStripBuscarButton;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripSaveButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGridViewCliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnNmCliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnNuTelefone;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnNuRg;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnNuCpf;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDsEndereco;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDtNascimento;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnIdEmpresa;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnNmUsuario;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDtAtualizacao;
    }
}