﻿namespace Voyager.Interface.Cadastro
{
    partial class FormFornecedor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormFornecedor));
            this.bindingNavigatorVoyager = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.toolStripBuscarButton = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSaveButton = new System.Windows.Forms.ToolStripButton();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxTipoFornecedor = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridViewFornecedor = new System.Windows.Forms.DataGridView();
            this.ColumnId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnNmFornecedor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDsEndereco = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnIdTipo = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ColumnNuTelefone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDsEmail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnNmResponsavel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDsBancaria = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnNmUsuario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDtAtualizacao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigatorVoyager)).BeginInit();
            this.bindingNavigatorVoyager.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFornecedor)).BeginInit();
            this.SuspendLayout();
            // 
            // bindingNavigatorVoyager
            // 
            this.bindingNavigatorVoyager.AddNewItem = this.bindingNavigatorAddNewItem;
            this.bindingNavigatorVoyager.CountItem = this.bindingNavigatorCountItem;
            this.bindingNavigatorVoyager.DeleteItem = this.bindingNavigatorDeleteItem;
            this.bindingNavigatorVoyager.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripBuscarButton,
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.toolStripSaveButton});
            this.bindingNavigatorVoyager.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigatorVoyager.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.bindingNavigatorVoyager.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.bindingNavigatorVoyager.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.bindingNavigatorVoyager.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.bindingNavigatorVoyager.Name = "bindingNavigatorVoyager";
            this.bindingNavigatorVoyager.PositionItem = this.bindingNavigatorPositionItem;
            this.bindingNavigatorVoyager.Size = new System.Drawing.Size(865, 25);
            this.bindingNavigatorVoyager.TabIndex = 4;
            this.bindingNavigatorVoyager.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Adicionar novo";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(37, 22);
            this.bindingNavigatorCountItem.Text = "de {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Número total de itens";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Excluir";
            // 
            // toolStripBuscarButton
            // 
            this.toolStripBuscarButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripBuscarButton.Image = ((System.Drawing.Image)(resources.GetObject("toolStripBuscarButton.Image")));
            this.toolStripBuscarButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripBuscarButton.Name = "toolStripBuscarButton";
            this.toolStripBuscarButton.Size = new System.Drawing.Size(23, 22);
            this.toolStripBuscarButton.Text = "toolStripButton1";
            this.toolStripBuscarButton.Click += new System.EventHandler(this.toolStripBuscarButton_Click);
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Mover primeiro";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Mover anterior";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Posição";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Posição atual";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Mover próximo";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Mover último";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSaveButton
            // 
            this.toolStripSaveButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripSaveButton.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSaveButton.Image")));
            this.toolStripSaveButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSaveButton.Name = "toolStripSaveButton";
            this.toolStripSaveButton.Size = new System.Drawing.Size(23, 22);
            this.toolStripSaveButton.Text = "toolStripButton1";
            this.toolStripSaveButton.Click += new System.EventHandler(this.toolStripSaveButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Tipo:";
            // 
            // comboBoxTipoFornecedor
            // 
            this.comboBoxTipoFornecedor.FormattingEnabled = true;
            this.comboBoxTipoFornecedor.Location = new System.Drawing.Point(43, 23);
            this.comboBoxTipoFornecedor.Name = "comboBoxTipoFornecedor";
            this.comboBoxTipoFornecedor.Size = new System.Drawing.Size(175, 21);
            this.comboBoxTipoFornecedor.TabIndex = 6;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.comboBoxTipoFornecedor);
            this.groupBox1.ForeColor = System.Drawing.Color.Teal;
            this.groupBox1.Location = new System.Drawing.Point(12, 28);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(231, 53);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filtro";
            // 
            // dataGridViewFornecedor
            // 
            this.dataGridViewFornecedor.AllowUserToAddRows = false;
            this.dataGridViewFornecedor.AllowUserToResizeColumns = false;
            this.dataGridViewFornecedor.AllowUserToResizeRows = false;
            this.dataGridViewFornecedor.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.dataGridViewFornecedor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewFornecedor.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnId,
            this.ColumnNmFornecedor,
            this.ColumnDsEndereco,
            this.ColumnIdTipo,
            this.ColumnNuTelefone,
            this.ColumnDsEmail,
            this.ColumnNmResponsavel,
            this.ColumnDsBancaria,
            this.ColumnNmUsuario,
            this.ColumnDtAtualizacao});
            this.dataGridViewFornecedor.Location = new System.Drawing.Point(12, 106);
            this.dataGridViewFornecedor.Name = "dataGridViewFornecedor";
            this.dataGridViewFornecedor.RowHeadersVisible = false;
            this.dataGridViewFornecedor.Size = new System.Drawing.Size(841, 280);
            this.dataGridViewFornecedor.TabIndex = 8;
            this.dataGridViewFornecedor.CurrentCellDirtyStateChanged += new System.EventHandler(this.dataGridViewFornecedor_CurrentCellDirtyStateChanged);
            // 
            // ColumnId
            // 
            this.ColumnId.DataPropertyName = "frn_id_fornecedor";
            this.ColumnId.HeaderText = "ColumnId";
            this.ColumnId.Name = "ColumnId";
            this.ColumnId.Visible = false;
            // 
            // ColumnNmFornecedor
            // 
            this.ColumnNmFornecedor.DataPropertyName = "frn_nm_nome";
            this.ColumnNmFornecedor.HeaderText = "Nome";
            this.ColumnNmFornecedor.Name = "ColumnNmFornecedor";
            // 
            // ColumnDsEndereco
            // 
            this.ColumnDsEndereco.DataPropertyName = "frn_ds_endereco";
            this.ColumnDsEndereco.HeaderText = "Endereço";
            this.ColumnDsEndereco.Name = "ColumnDsEndereco";
            // 
            // ColumnIdTipo
            // 
            this.ColumnIdTipo.DataPropertyName = "frn_id_tipo_fornecedor";
            this.ColumnIdTipo.HeaderText = "Tipo de Fornecedor";
            this.ColumnIdTipo.Name = "ColumnIdTipo";
            // 
            // ColumnNuTelefone
            // 
            this.ColumnNuTelefone.DataPropertyName = "frn_nu_telefone";
            this.ColumnNuTelefone.HeaderText = "Telefone";
            this.ColumnNuTelefone.Name = "ColumnNuTelefone";
            this.ColumnNuTelefone.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColumnNuTelefone.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ColumnDsEmail
            // 
            this.ColumnDsEmail.DataPropertyName = "frn_ds_email";
            this.ColumnDsEmail.HeaderText = "E-mail";
            this.ColumnDsEmail.Name = "ColumnDsEmail";
            this.ColumnDsEmail.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColumnDsEmail.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ColumnNmResponsavel
            // 
            this.ColumnNmResponsavel.DataPropertyName = "frn_nm_responsavel";
            this.ColumnNmResponsavel.HeaderText = "Responsável";
            this.ColumnNmResponsavel.Name = "ColumnNmResponsavel";
            this.ColumnNmResponsavel.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColumnNmResponsavel.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ColumnDsBancaria
            // 
            this.ColumnDsBancaria.DataPropertyName = "frn_ds_bancaria";
            this.ColumnDsBancaria.HeaderText = "Dados Bancários";
            this.ColumnDsBancaria.Name = "ColumnDsBancaria";
            this.ColumnDsBancaria.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColumnDsBancaria.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ColumnNmUsuario
            // 
            this.ColumnNmUsuario.DataPropertyName = "frn_nm_usuario";
            this.ColumnNmUsuario.HeaderText = "ColumnNmUsuario";
            this.ColumnNmUsuario.Name = "ColumnNmUsuario";
            this.ColumnNmUsuario.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColumnNmUsuario.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColumnNmUsuario.Visible = false;
            // 
            // ColumnDtAtualizacao
            // 
            this.ColumnDtAtualizacao.DataPropertyName = "frn_dt_atualizacao";
            this.ColumnDtAtualizacao.HeaderText = "ColumnDtAtualizacao";
            this.ColumnDtAtualizacao.Name = "ColumnDtAtualizacao";
            this.ColumnDtAtualizacao.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColumnDtAtualizacao.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColumnDtAtualizacao.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.ForeColor = System.Drawing.Color.Teal;
            this.groupBox2.Location = new System.Drawing.Point(14, 87);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(831, 10);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Teal;
            this.label2.Location = new System.Drawing.Point(22, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Registros";
            // 
            // FormFornecedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(865, 398);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.dataGridViewFornecedor);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.bindingNavigatorVoyager);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormFornecedor";
            this.Text = "Cadastro - Fornecedor";
            this.Resize += new System.EventHandler(this.RedimensionarTela);
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigatorVoyager)).EndInit();
            this.bindingNavigatorVoyager.ResumeLayout(false);
            this.bindingNavigatorVoyager.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFornecedor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingNavigator bindingNavigatorVoyager;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton toolStripBuscarButton;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripSaveButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxTipoFornecedor;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridViewFornecedor;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnNmFornecedor;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDsEndereco;
        private System.Windows.Forms.DataGridViewComboBoxColumn ColumnIdTipo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnNuTelefone;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDsEmail;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnNmResponsavel;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDsBancaria;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnNmUsuario;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDtAtualizacao;
    }
}