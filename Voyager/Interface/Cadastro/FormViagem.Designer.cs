﻿namespace Voyager.Interface.Cadastro
{
    partial class FormViagem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormViagem));
            this.bindingNavigatorVoyager = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.toolStripBuscarButton = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSaveButton = new System.Windows.Forms.ToolStripButton();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxCidade = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dateTimePickerInicial = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.dateTimePickerFinal = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dataGridViewViagem = new System.Windows.Forms.DataGridView();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageCliente = new System.Windows.Forms.TabPage();
            this.buttonExcluir = new System.Windows.Forms.Button();
            this.buttonAdicionar = new System.Windows.Forms.Button();
            this.dataGridViewClienteViagem = new System.Windows.Forms.DataGridView();
            this.ColumnId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnIdFornecedor = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ColumnIdCidade = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ColumnDtIda = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDtVolta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnVlUnitario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnIdEmpresa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnNmUsuario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDtAtualizacao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnIdClienteViagem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnIdViagem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnIdCliente = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ColumnNmUsuarioClv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDtAtualizacaoClv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigatorVoyager)).BeginInit();
            this.bindingNavigatorVoyager.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewViagem)).BeginInit();
            this.tabControl.SuspendLayout();
            this.tabPageCliente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewClienteViagem)).BeginInit();
            this.SuspendLayout();
            // 
            // bindingNavigatorVoyager
            // 
            this.bindingNavigatorVoyager.AddNewItem = this.bindingNavigatorAddNewItem;
            this.bindingNavigatorVoyager.CountItem = this.bindingNavigatorCountItem;
            this.bindingNavigatorVoyager.DeleteItem = this.bindingNavigatorDeleteItem;
            this.bindingNavigatorVoyager.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripBuscarButton,
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.toolStripSaveButton});
            this.bindingNavigatorVoyager.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigatorVoyager.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.bindingNavigatorVoyager.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.bindingNavigatorVoyager.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.bindingNavigatorVoyager.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.bindingNavigatorVoyager.Name = "bindingNavigatorVoyager";
            this.bindingNavigatorVoyager.PositionItem = this.bindingNavigatorPositionItem;
            this.bindingNavigatorVoyager.Size = new System.Drawing.Size(731, 25);
            this.bindingNavigatorVoyager.TabIndex = 5;
            this.bindingNavigatorVoyager.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Adicionar novo";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(37, 22);
            this.bindingNavigatorCountItem.Text = "de {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Número total de itens";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Excluir";
            // 
            // toolStripBuscarButton
            // 
            this.toolStripBuscarButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripBuscarButton.Image = ((System.Drawing.Image)(resources.GetObject("toolStripBuscarButton.Image")));
            this.toolStripBuscarButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripBuscarButton.Name = "toolStripBuscarButton";
            this.toolStripBuscarButton.Size = new System.Drawing.Size(23, 22);
            this.toolStripBuscarButton.Text = "toolStripButton1";
            this.toolStripBuscarButton.Click += new System.EventHandler(this.toolStripBuscarButton_Click);
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Mover primeiro";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Mover anterior";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Posição";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Posição atual";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Mover próximo";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Mover último";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSaveButton
            // 
            this.toolStripSaveButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripSaveButton.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSaveButton.Image")));
            this.toolStripSaveButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSaveButton.Name = "toolStripSaveButton";
            this.toolStripSaveButton.Size = new System.Drawing.Size(23, 22);
            this.toolStripSaveButton.Text = "toolStripButton1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 81);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Cidade:";
            // 
            // comboBoxCidade
            // 
            this.comboBoxCidade.FormattingEnabled = true;
            this.comboBoxCidade.Location = new System.Drawing.Point(51, 78);
            this.comboBoxCidade.Name = "comboBoxCidade";
            this.comboBoxCidade.Size = new System.Drawing.Size(175, 21);
            this.comboBoxCidade.TabIndex = 6;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.comboBoxCidade);
            this.groupBox1.ForeColor = System.Drawing.Color.Teal;
            this.groupBox1.Location = new System.Drawing.Point(12, 28);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(257, 112);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filtro";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dateTimePickerInicial);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.dateTimePickerFinal);
            this.groupBox2.ForeColor = System.Drawing.Color.Teal;
            this.groupBox2.Location = new System.Drawing.Point(9, 19);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(233, 50);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Período";
            // 
            // dateTimePickerInicial
            // 
            this.dateTimePickerInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerInicial.Location = new System.Drawing.Point(6, 19);
            this.dateTimePickerInicial.Name = "dateTimePickerInicial";
            this.dateTimePickerInicial.Size = new System.Drawing.Size(96, 20);
            this.dateTimePickerInicial.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(108, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(13, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "a";
            // 
            // dateTimePickerFinal
            // 
            this.dateTimePickerFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerFinal.Location = new System.Drawing.Point(127, 19);
            this.dateTimePickerFinal.Name = "dateTimePickerFinal";
            this.dateTimePickerFinal.Size = new System.Drawing.Size(96, 20);
            this.dateTimePickerFinal.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Teal;
            this.label2.Location = new System.Drawing.Point(22, 146);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Registros";
            // 
            // groupBox3
            // 
            this.groupBox3.ForeColor = System.Drawing.Color.Teal;
            this.groupBox3.Location = new System.Drawing.Point(12, 146);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(596, 10);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            // 
            // dataGridViewViagem
            // 
            this.dataGridViewViagem.AllowUserToAddRows = false;
            this.dataGridViewViagem.AllowUserToResizeColumns = false;
            this.dataGridViewViagem.AllowUserToResizeRows = false;
            this.dataGridViewViagem.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.dataGridViewViagem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewViagem.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnId,
            this.ColumnIdFornecedor,
            this.ColumnIdCidade,
            this.ColumnDtIda,
            this.ColumnDtVolta,
            this.ColumnVlUnitario,
            this.ColumnIdEmpresa,
            this.ColumnNmUsuario,
            this.ColumnDtAtualizacao});
            this.dataGridViewViagem.Location = new System.Drawing.Point(12, 165);
            this.dataGridViewViagem.Name = "dataGridViewViagem";
            this.dataGridViewViagem.RowHeadersVisible = false;
            this.dataGridViewViagem.Size = new System.Drawing.Size(592, 132);
            this.dataGridViewViagem.TabIndex = 13;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPageCliente);
            this.tabControl.Location = new System.Drawing.Point(12, 320);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(316, 263);
            this.tabControl.TabIndex = 14;
            // 
            // tabPageCliente
            // 
            this.tabPageCliente.Controls.Add(this.buttonExcluir);
            this.tabPageCliente.Controls.Add(this.buttonAdicionar);
            this.tabPageCliente.Controls.Add(this.dataGridViewClienteViagem);
            this.tabPageCliente.Location = new System.Drawing.Point(4, 22);
            this.tabPageCliente.Name = "tabPageCliente";
            this.tabPageCliente.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageCliente.Size = new System.Drawing.Size(308, 237);
            this.tabPageCliente.TabIndex = 0;
            this.tabPageCliente.Text = "Clientes";
            this.tabPageCliente.UseVisualStyleBackColor = true;
            // 
            // buttonExcluir
            // 
            this.buttonExcluir.Location = new System.Drawing.Point(213, 208);
            this.buttonExcluir.Name = "buttonExcluir";
            this.buttonExcluir.Size = new System.Drawing.Size(75, 23);
            this.buttonExcluir.TabIndex = 2;
            this.buttonExcluir.Text = "Excluir";
            this.buttonExcluir.UseVisualStyleBackColor = true;
            // 
            // buttonAdicionar
            // 
            this.buttonAdicionar.Location = new System.Drawing.Point(132, 208);
            this.buttonAdicionar.Name = "buttonAdicionar";
            this.buttonAdicionar.Size = new System.Drawing.Size(75, 23);
            this.buttonAdicionar.TabIndex = 1;
            this.buttonAdicionar.Text = "Adicionar";
            this.buttonAdicionar.UseVisualStyleBackColor = true;
            // 
            // dataGridViewClienteViagem
            // 
            this.dataGridViewClienteViagem.AllowUserToAddRows = false;
            this.dataGridViewClienteViagem.AllowUserToResizeColumns = false;
            this.dataGridViewClienteViagem.AllowUserToResizeRows = false;
            this.dataGridViewClienteViagem.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.dataGridViewClienteViagem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewClienteViagem.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnIdClienteViagem,
            this.ColumnIdViagem,
            this.ColumnIdCliente,
            this.ColumnNmUsuarioClv,
            this.ColumnDtAtualizacaoClv});
            this.dataGridViewClienteViagem.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewClienteViagem.Name = "dataGridViewClienteViagem";
            this.dataGridViewClienteViagem.RowHeadersVisible = false;
            this.dataGridViewClienteViagem.Size = new System.Drawing.Size(298, 199);
            this.dataGridViewClienteViagem.TabIndex = 0;
            // 
            // ColumnId
            // 
            this.ColumnId.DataPropertyName = "via_id_viagem";
            this.ColumnId.HeaderText = "ColumnId";
            this.ColumnId.Name = "ColumnId";
            this.ColumnId.Visible = false;
            // 
            // ColumnIdFornecedor
            // 
            this.ColumnIdFornecedor.DataPropertyName = "via_id_fornecedor";
            this.ColumnIdFornecedor.HeaderText = "Fornecedor";
            this.ColumnIdFornecedor.Name = "ColumnIdFornecedor";
            this.ColumnIdFornecedor.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColumnIdFornecedor.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // ColumnIdCidade
            // 
            this.ColumnIdCidade.DataPropertyName = "via_id_cidade";
            this.ColumnIdCidade.HeaderText = "Cidade";
            this.ColumnIdCidade.Name = "ColumnIdCidade";
            this.ColumnIdCidade.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColumnIdCidade.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // ColumnDtIda
            // 
            this.ColumnDtIda.DataPropertyName = "via_dt_ida";
            this.ColumnDtIda.HeaderText = "Data de Ida";
            this.ColumnDtIda.Name = "ColumnDtIda";
            // 
            // ColumnDtVolta
            // 
            this.ColumnDtVolta.DataPropertyName = "via_dt_volta";
            this.ColumnDtVolta.HeaderText = "Data de Volta";
            this.ColumnDtVolta.Name = "ColumnDtVolta";
            // 
            // ColumnVlUnitario
            // 
            this.ColumnVlUnitario.DataPropertyName = "via_vl_unitario";
            this.ColumnVlUnitario.HeaderText = "Valor Unitário(R$)";
            this.ColumnVlUnitario.Name = "ColumnVlUnitario";
            // 
            // ColumnIdEmpresa
            // 
            this.ColumnIdEmpresa.DataPropertyName = "via_id_empresa";
            this.ColumnIdEmpresa.HeaderText = "ColumnIdEmpresa";
            this.ColumnIdEmpresa.Name = "ColumnIdEmpresa";
            this.ColumnIdEmpresa.Visible = false;
            // 
            // ColumnNmUsuario
            // 
            this.ColumnNmUsuario.DataPropertyName = "via_nm_usuario";
            this.ColumnNmUsuario.HeaderText = "ColumnNmUsuario";
            this.ColumnNmUsuario.Name = "ColumnNmUsuario";
            this.ColumnNmUsuario.Visible = false;
            // 
            // ColumnDtAtualizacao
            // 
            this.ColumnDtAtualizacao.DataPropertyName = "via_dt_atualizacao";
            this.ColumnDtAtualizacao.HeaderText = "ColumnDtAtualizacao";
            this.ColumnDtAtualizacao.Name = "ColumnDtAtualizacao";
            this.ColumnDtAtualizacao.Visible = false;
            // 
            // ColumnIdClienteViagem
            // 
            this.ColumnIdClienteViagem.DataPropertyName = "clv_id_cliente_viagem";
            this.ColumnIdClienteViagem.HeaderText = "ColumnIdClienteViagem";
            this.ColumnIdClienteViagem.Name = "ColumnIdClienteViagem";
            this.ColumnIdClienteViagem.Visible = false;
            // 
            // ColumnIdViagem
            // 
            this.ColumnIdViagem.DataPropertyName = "clv_id_viagem";
            this.ColumnIdViagem.HeaderText = "ColumnIdViagem";
            this.ColumnIdViagem.Name = "ColumnIdViagem";
            this.ColumnIdViagem.Visible = false;
            // 
            // ColumnIdCliente
            // 
            this.ColumnIdCliente.DataPropertyName = "clv_id_cliente";
            this.ColumnIdCliente.HeaderText = "Nome";
            this.ColumnIdCliente.Name = "ColumnIdCliente";
            this.ColumnIdCliente.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColumnIdCliente.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ColumnIdCliente.Width = 250;
            // 
            // ColumnNmUsuarioClv
            // 
            this.ColumnNmUsuarioClv.DataPropertyName = "clv_nm_usuario";
            this.ColumnNmUsuarioClv.HeaderText = "ColumnNmUsuarioClv";
            this.ColumnNmUsuarioClv.Name = "ColumnNmUsuarioClv";
            this.ColumnNmUsuarioClv.Visible = false;
            // 
            // ColumnDtAtualizacaoClv
            // 
            this.ColumnDtAtualizacaoClv.DataPropertyName = "clv_dt_atualizacao";
            this.ColumnDtAtualizacaoClv.HeaderText = "ColumnDtAtualizacaoClv";
            this.ColumnDtAtualizacaoClv.Name = "ColumnDtAtualizacaoClv";
            this.ColumnDtAtualizacaoClv.Visible = false;
            // 
            // FormViagem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(731, 590);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.dataGridViewViagem);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.bindingNavigatorVoyager);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormViagem";
            this.Text = "Cadastro - Viagem";
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigatorVoyager)).EndInit();
            this.bindingNavigatorVoyager.ResumeLayout(false);
            this.bindingNavigatorVoyager.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewViagem)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.tabPageCliente.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewClienteViagem)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingNavigator bindingNavigatorVoyager;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton toolStripBuscarButton;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripSaveButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxCidade;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker dateTimePickerInicial;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dateTimePickerFinal;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView dataGridViewViagem;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageCliente;
        private System.Windows.Forms.Button buttonExcluir;
        private System.Windows.Forms.Button buttonAdicionar;
        private System.Windows.Forms.DataGridView dataGridViewClienteViagem;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnId;
        private System.Windows.Forms.DataGridViewComboBoxColumn ColumnIdFornecedor;
        private System.Windows.Forms.DataGridViewComboBoxColumn ColumnIdCidade;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDtIda;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDtVolta;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnVlUnitario;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnIdEmpresa;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnNmUsuario;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDtAtualizacao;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnIdClienteViagem;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnIdViagem;
        private System.Windows.Forms.DataGridViewComboBoxColumn ColumnIdCliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnNmUsuarioClv;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDtAtualizacaoClv;
    }
}