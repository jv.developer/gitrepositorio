﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Voyager.Interface.Cadastro
{
    public partial class FormFornecedor : Form
    {
        DataSet ioDataSetVoyager;
        SqlDataAdapter ioSqlDataAdapterFornecedor;
        BindingSource ioBindingSourceFornecedor;
        int ii_width_old = 0;
        int ii_heigth_old = 0;
        SqlConnection loConexao;

        public FormFornecedor()
        {
            InitializeComponent();
            loConexao = GerenciadorAplicacao.ConexaoBancoDados.DbConnectionStringBuilder;
            this.ArmazenarAlturaLarguraTela();
            this.dataGridViewFornecedor.AutoResizeColumns();
            this.dataGridViewFornecedor.AutoSizeColumnsMode =
                DataGridViewAutoSizeColumnsMode.AllCells;
            DefinirCbFiltroTipoFornecedor();
            DefinirComboBoxTipoFornecedor();
        }

        /// <summary>
        /// Eventos de clique dos botões do menu...
        /// </summary>

        private void toolStripBuscarButton_Click(object sender, EventArgs e)
        {
            ExecutarConsultaFornecedor();
        }

        private void toolStripSaveButton_Click(object sender, EventArgs e)
        {
            dataGridViewFornecedor.CommitEdit(DataGridViewDataErrorContexts.Commit);
            this.ioBindingSourceFornecedor.EndEdit();
            for (int i = 0; i < dataGridViewFornecedor.RowCount; i++)
            {
                DataGridViewRow gridrow = dataGridViewFornecedor.Rows[i];
                DataRowView rowview = (DataRowView)gridrow.DataBoundItem;
                DataRow row = rowview.Row;

                if (string.IsNullOrEmpty(dataGridViewFornecedor.Rows[i].Cells["ColumnNmFornecedor"].Value.ToString()))
                {
                    MessageBox.Show("Nenhum nome informado!");
                    return;
                }

                if (row.RowState.Equals(DataRowState.Added))
                {
                    dataGridViewFornecedor.Rows[i].Cells["ColumnNmUsuario"].Value = GerenciadorAplicacao.UsuarioAtual.Login;
                    dataGridViewFornecedor.Rows[i].Cells["ColumnDtAtualizacao"].Value = DateTime.Today;
                }
                else if (row.RowState.Equals(DataRowState.Modified))
                {
                    dataGridViewFornecedor.Rows[i].Cells["ColumnNmUsuario"].Value = GerenciadorAplicacao.UsuarioAtual.Login;
                    dataGridViewFornecedor.Rows[i].Cells["ColumnDtAtualizacao"].Value = DateTime.Today;
                }
            }

            try
            {
                if (this.ioSqlDataAdapterFornecedor.Update((DataTable)this.ioBindingSourceFornecedor.DataSource) >= 1)
                {
                    MessageBox.Show("Operação executada com sucesso!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.ToString());
            }
            dataGridViewFornecedor.Refresh();
        }

        /// <summary>
        /// Metodos de execução no banco de dados, definição de scripts de conexão e parametros... 
        /// </summary>

        private void ExecutarConsultaFornecedor()
        {
            SqlCommand loSqlCommandSelect = ObterSqlCommandFornecedor();

            this.ioSqlDataAdapterFornecedor = new SqlDataAdapter(loSqlCommandSelect);
            SqlCommandBuilder loSqlCommandBuilder = new SqlCommandBuilder(this.ioSqlDataAdapterFornecedor);
            this.ioDataSetVoyager = new DataSet();
            this.ioSqlDataAdapterFornecedor.Fill(this.ioDataSetVoyager);
            this.ioBindingSourceFornecedor = new BindingSource();
            this.ioBindingSourceFornecedor.DataSource = this.ioDataSetVoyager.Tables[0];
            this.bindingNavigatorVoyager.BindingSource = this.ioBindingSourceFornecedor;
            this.dataGridViewFornecedor.DataSource = this.ioBindingSourceFornecedor;
        }

        private SqlCommand ObterSqlCommandFornecedor()
        {
            SqlCommand loSqlCommandFornecedor = new SqlCommand(
                "SELECT frn_id_fornecedor, " +
                "frn_nm_nome, " +
                "frn_ds_endereco, " +
                "frn_id_tipo_fornecedor, " +
                "frn_nu_telefone, " +
                "frn_ds_email, " +
                "frn_nm_responsavel, " +
                "frn_ds_bancaria, " +
                "frn_nm_usuario, " +
                "frn_dt_atualizacao " +
                "FROM FRN_fornecedor " +
                "WHERE frn_id_tipo_fornecedor = @idTipoFornecedor or @idTipoFornecedor = -1", loConexao);

            loSqlCommandFornecedor.Parameters.Add(GerarParametroFornecedor("@idTipoFornecedor", Convert.ToInt32(this.comboBoxTipoFornecedor.SelectedValue)));

            return loSqlCommandFornecedor;
        }

        private SqlParameter GerarParametroFornecedor(string nomeParametro, object value)
        {
            SqlParameter loSqlParameterFornecedor = new SqlParameter();
            loSqlParameterFornecedor.ParameterName = nomeParametro;
            loSqlParameterFornecedor.Value = value;
            return loSqlParameterFornecedor;
        }


        /// <summary>
        /// Metodos de definição de ComboBox/Colunas do DataGridView...
        /// </summary>

        private void DefinirComboBoxTipoFornecedor()
        {
            SqlCommand loSqlCommandCbColuna = new SqlCommand(
                "SELECT (tfn_id_tipo_fornecedor) cd_dado, " +
                "(tfn_nm_tipo) ds_exibicao " +
                "FROM TFN_tipo_fornecedor", loConexao);

            SqlDataAdapter loSqlDataAdapterCbColuna = new SqlDataAdapter(loSqlCommandCbColuna);
            DataSet loDataSetCbColuna = new DataSet();
            loSqlDataAdapterCbColuna.Fill(loDataSetCbColuna);
            this.ColumnIdTipo.DisplayMember = "ds_exibicao";
            this.ColumnIdTipo.ValueMember = "cd_dado";
            this.ColumnIdTipo.DataSource = loDataSetCbColuna.Tables[0];
        }
        /// <summary>
        /// Metodos de definição dos filtros de busca..
        /// </summary>

        private void DefinirCbFiltroTipoFornecedor()
        {
            SqlCommand loSqlCommandCbFiltro = new SqlCommand(
                "SELECT (tfn_id_tipo_fornecedor) cd_dado, " +
                "(tfn_nm_tipo) ds_exibicao " +
                "FROM TFN_tipo_fornecedor " +
                "UNION " +
                "SELECT -1, '[TODOS]' " +
                "ORDER BY ds_exibicao", loConexao);

            SqlDataAdapter loSqlDataAdapterCbFiltroPropriedade = new SqlDataAdapter(loSqlCommandCbFiltro);
            DataSet loDataSetCbFiltroPropriedade = new DataSet();
            loSqlDataAdapterCbFiltroPropriedade.Fill(loDataSetCbFiltroPropriedade);
            this.comboBoxTipoFornecedor.DisplayMember = "ds_exibicao";
            this.comboBoxTipoFornecedor.ValueMember = "cd_dado";
            this.comboBoxTipoFornecedor.DataSource = loDataSetCbFiltroPropriedade.Tables[0];
        }
        /// <summary>
        /// Metodos de redimensionamento da tela e persistência das informações ao adicionar uma nova linha....
        /// <summary>

        private void RedimensionarTela(object sender, EventArgs e)
        {
            if (WindowState != FormWindowState.Minimized)
            {
                Size tempSize = this.Size;
                tempSize.Height = this.dataGridViewFornecedor.Size.Height + this.Size.Height - ii_heigth_old;
                tempSize.Width = this.dataGridViewFornecedor.Size.Width + this.Size.Width - ii_width_old;
                ArmazenarAlturaLarguraTela();
                this.dataGridViewFornecedor.Size = tempSize;
            }
        }

        private void ArmazenarAlturaLarguraTela()
        {
            ii_width_old = this.Size.Width;
            ii_heigth_old = this.Size.Height;
        }

        private void dataGridViewFornecedor_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            dataGridViewFornecedor.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

    }
}
