﻿namespace Voyager.Interface.Tabela
{
    partial class FormTabCidade
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormTabCidade));
            this.dataGridViewCidade = new System.Windows.Forms.DataGridView();
            this.bindingNavigatorVoyager = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.toolStripBuscarButton = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSaveButton = new System.Windows.Forms.ToolStripButton();
            this.comboBoxEstado = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ColumnId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnNmCidade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnIdEstado = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ColumnCdIbge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCidade)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigatorVoyager)).BeginInit();
            this.bindingNavigatorVoyager.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridViewCidade
            // 
            this.dataGridViewCidade.AllowUserToAddRows = false;
            this.dataGridViewCidade.AllowUserToResizeColumns = false;
            this.dataGridViewCidade.AllowUserToResizeRows = false;
            this.dataGridViewCidade.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.dataGridViewCidade.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCidade.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnId,
            this.ColumnNmCidade,
            this.ColumnIdEstado,
            this.ColumnCdIbge});
            this.dataGridViewCidade.Location = new System.Drawing.Point(14, 74);
            this.dataGridViewCidade.Name = "dataGridViewCidade";
            this.dataGridViewCidade.RowHeadersVisible = false;
            this.dataGridViewCidade.Size = new System.Drawing.Size(469, 254);
            this.dataGridViewCidade.TabIndex = 1;
            this.dataGridViewCidade.CurrentCellDirtyStateChanged += new System.EventHandler(this.dataGridViewCidade_CurrentCellDirtyStateChanged);
            // 
            // bindingNavigatorVoyager
            // 
            this.bindingNavigatorVoyager.AddNewItem = this.bindingNavigatorAddNewItem;
            this.bindingNavigatorVoyager.CountItem = this.bindingNavigatorCountItem;
            this.bindingNavigatorVoyager.DeleteItem = this.bindingNavigatorDeleteItem;
            this.bindingNavigatorVoyager.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripBuscarButton,
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.toolStripSaveButton});
            this.bindingNavigatorVoyager.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigatorVoyager.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.bindingNavigatorVoyager.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.bindingNavigatorVoyager.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.bindingNavigatorVoyager.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.bindingNavigatorVoyager.Name = "bindingNavigatorVoyager";
            this.bindingNavigatorVoyager.PositionItem = this.bindingNavigatorPositionItem;
            this.bindingNavigatorVoyager.Size = new System.Drawing.Size(495, 25);
            this.bindingNavigatorVoyager.TabIndex = 2;
            this.bindingNavigatorVoyager.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Adicionar novo";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(37, 22);
            this.bindingNavigatorCountItem.Text = "de {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Número total de itens";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Excluir";
            // 
            // toolStripBuscarButton
            // 
            this.toolStripBuscarButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripBuscarButton.Image = ((System.Drawing.Image)(resources.GetObject("toolStripBuscarButton.Image")));
            this.toolStripBuscarButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripBuscarButton.Name = "toolStripBuscarButton";
            this.toolStripBuscarButton.Size = new System.Drawing.Size(23, 22);
            this.toolStripBuscarButton.Text = "toolStripButton1";
            this.toolStripBuscarButton.Click += new System.EventHandler(this.toolStripBuscarButton_Click);
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Mover primeiro";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Mover anterior";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Posição";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Posição atual";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Mover próximo";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Mover último";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSaveButton
            // 
            this.toolStripSaveButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripSaveButton.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSaveButton.Image")));
            this.toolStripSaveButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSaveButton.Name = "toolStripSaveButton";
            this.toolStripSaveButton.Size = new System.Drawing.Size(23, 22);
            this.toolStripSaveButton.Text = "toolStripButton1";
            this.toolStripSaveButton.Click += new System.EventHandler(this.toolStripSaveButton_Click);
            // 
            // comboBoxEstado
            // 
            this.comboBoxEstado.FormattingEnabled = true;
            this.comboBoxEstado.Location = new System.Drawing.Point(76, 37);
            this.comboBoxEstado.Name = "comboBoxEstado";
            this.comboBoxEstado.Size = new System.Drawing.Size(139, 21);
            this.comboBoxEstado.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Estado:";
            // 
            // ColumnId
            // 
            this.ColumnId.DataPropertyName = "cid_id_cidade";
            this.ColumnId.HeaderText = "ColumnId";
            this.ColumnId.Name = "ColumnId";
            this.ColumnId.Visible = false;
            // 
            // ColumnNmCidade
            // 
            this.ColumnNmCidade.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ColumnNmCidade.DataPropertyName = "cid_nm_cidade";
            this.ColumnNmCidade.HeaderText = "Cidade";
            this.ColumnNmCidade.Name = "ColumnNmCidade";
            this.ColumnNmCidade.Width = 200;
            // 
            // ColumnIdEstado
            // 
            this.ColumnIdEstado.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ColumnIdEstado.DataPropertyName = "cid_id_estado";
            this.ColumnIdEstado.HeaderText = "Estado";
            this.ColumnIdEstado.Name = "ColumnIdEstado";
            this.ColumnIdEstado.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColumnIdEstado.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ColumnIdEstado.Width = 140;
            // 
            // ColumnCdIbge
            // 
            this.ColumnCdIbge.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ColumnCdIbge.DataPropertyName = "cid_cd_cidade_ibge";
            this.ColumnCdIbge.HeaderText = "Código IBGE";
            this.ColumnCdIbge.Name = "ColumnCdIbge";
            this.ColumnCdIbge.Width = 95;
            // 
            // FormTabCidade
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(495, 340);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxEstado);
            this.Controls.Add(this.bindingNavigatorVoyager);
            this.Controls.Add(this.dataGridViewCidade);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormTabCidade";
            this.Text = "Tabela - Cidade";
            this.Resize += new System.EventHandler(this.RedimensionarTela);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCidade)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigatorVoyager)).EndInit();
            this.bindingNavigatorVoyager.ResumeLayout(false);
            this.bindingNavigatorVoyager.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dataGridViewCidade;
        private System.Windows.Forms.BindingNavigator bindingNavigatorVoyager;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripBuscarButton;
        private System.Windows.Forms.ToolStripButton toolStripSaveButton;
        private System.Windows.Forms.ComboBox comboBoxEstado;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnNmCidade;
        private System.Windows.Forms.DataGridViewComboBoxColumn ColumnIdEstado;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnCdIbge;
    }
}