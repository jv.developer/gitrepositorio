﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Voyager.Interface.Tabela
{
    public partial class FormTabCidade : Form
    {
        DataSet ioDataSetVoyager;
        SqlDataAdapter ioSqlDataAdapterCidade;
        BindingSource ioBindingSourceCidade;
        int ii_width_old = 0;
        int ii_heigth_old = 0;
        SqlConnection loConexao;

        public FormTabCidade()
        {
            InitializeComponent();
            loConexao = GerenciadorAplicacao.ConexaoBancoDados.DbConnectionStringBuilder;
            this.ArmazenarAlturaLarguraTela();
            this.dataGridViewCidade.AutoResizeColumns();
            this.dataGridViewCidade.AutoSizeColumnsMode =
                DataGridViewAutoSizeColumnsMode.AllCells;
            DefinirCbFiltroEstado();
            DefinirComboBoxEstado();
        }

        /// <summary>
        /// Eventos de clique dos botões do menu...
        /// </summary>

        private void toolStripBuscarButton_Click(object sender, EventArgs e)
        {
            ExecutarConsultaCidade();
        }

        private void toolStripSaveButton_Click(object sender, EventArgs e)
        {
            dataGridViewCidade.CommitEdit(DataGridViewDataErrorContexts.Commit);
            this.ioBindingSourceCidade.EndEdit();
            Int32 liContInsertCodigo = 0;
            for (int i = 0; i < dataGridViewCidade.RowCount; i++)
            {
                DataGridViewRow gridrow = dataGridViewCidade.Rows[i];
                DataRowView rowview = (DataRowView)gridrow.DataBoundItem;
                DataRow row = rowview.Row;

                string lsNmCidade = dataGridViewCidade.Rows[i].Cells["ColumnNmCidade"].Value.ToString();
                if (row.RowState.Equals(DataRowState.Added))
                {
                    if (string.IsNullOrEmpty(lsNmCidade) || dataGridViewCidade.Rows[i].Cells["ColumnIdEstado"].Value.ToString().Equals(""))
                    {
                        MessageBox.Show("Informação obrigatoria não preenchida");
                        return;
                    }
                    liContInsertCodigo++;
                    dataGridViewCidade.Rows[i].Cells["ColumnId"].Value = GerarChavePrimaria() + liContInsertCodigo;
                }
            }

            try
            {
                if (this.ioSqlDataAdapterCidade.Update((DataTable)this.ioBindingSourceCidade.DataSource) >= 1)
                {
                    MessageBox.Show("Operação executada com sucesso!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.ToString());
            }

            dataGridViewCidade.Refresh();
        }

        /// <summary>
        /// Metodos de execução no banco de dados, definição de scripts de conexão e parametros...
        /// </summary>

        private void ExecutarConsultaCidade()
        {
            SqlCommand loSqlCommandSelectCidade = ObterSqlCommandSelect();

            this.ioSqlDataAdapterCidade = new SqlDataAdapter(loSqlCommandSelectCidade);
            SqlCommandBuilder loSqlCommandBuilderCidade = new SqlCommandBuilder(this.ioSqlDataAdapterCidade);
            this.ioDataSetVoyager = new DataSet();
            this.ioSqlDataAdapterCidade.Fill(this.ioDataSetVoyager);
            this.ioBindingSourceCidade = new BindingSource();
            this.ioBindingSourceCidade.DataSource = this.ioDataSetVoyager.Tables[0];
            this.bindingNavigatorVoyager.BindingSource = this.ioBindingSourceCidade;
            this.dataGridViewCidade.DataSource = this.ioBindingSourceCidade;
        }

        private SqlCommand ObterSqlCommandSelect()
        {
            SqlCommand loSqlCommandCid = new SqlCommand(
                "SELECT cid_id_cidade, " +
                "cid_nm_cidade, " +
                "cid_id_estado," +
                "cid_cd_cidade_ibge " +
                "FROM CID_cidade " +
                "WHERE cid_id_estado = @idEstado or @idEstado = -1 " +
                "ORDER BY cid_nm_cidade ASC", loConexao);

            loSqlCommandCid.Parameters.Add(GerarParametroCidade("@idEstado", Convert.ToInt32(this.comboBoxEstado.SelectedValue)));

            return loSqlCommandCid;
        }

        private SqlParameter GerarParametroCidade(string nomeParametro, object value)
        {
            SqlParameter loSqlParameterCidade = new SqlParameter();
            loSqlParameterCidade.ParameterName = nomeParametro;
            loSqlParameterCidade.Value = value;
            return loSqlParameterCidade;
        }

        private Int32 GerarChavePrimaria()
        {
            SqlCommand loSqlCommandChaveCid = new SqlCommand(
                "SELECT (ISNULL(MAX(cid_id_cidade),0)) " +
                "FROM CID_cidade", loConexao);

            SqlDataAdapter loSqlDataAdapterChave = new SqlDataAdapter(loSqlCommandChaveCid);
            DataSet loDataSetChave = new DataSet();
            loSqlDataAdapterChave.Fill(loDataSetChave);
            Int32 retornoConsulta = Convert.ToInt32(loDataSetChave.Tables[0].Rows[0].ItemArray[0]);
            return retornoConsulta;
        }

        /// <summary>
		/// Metodos de definição de ComboBox/Colunas do DataGridView...
		/// </summary>
        
        private void DefinirComboBoxEstado()
        {
            SqlCommand loSqlCommandCbEstado = new SqlCommand(
                "SELECT (est_nm_nome) ds_exibicao, " +
                "(est_id_estado) cd_dado " +
                "FROM EST_estado " +
                "ORDER BY ds_exibicao ASC", loConexao);

            SqlDataAdapter loSqlDataAdapterCbColuna = new SqlDataAdapter(loSqlCommandCbEstado);
            SqlCommandBuilder loSqlCommandBuilderCbColuna = new SqlCommandBuilder(loSqlDataAdapterCbColuna);
            DataSet loDataSetCbColuna = new DataSet();
            loSqlDataAdapterCbColuna.Fill(loDataSetCbColuna);
            this.ColumnIdEstado.DisplayMember = "ds_exibicao";
            this.ColumnIdEstado.ValueMember = "cd_dado";
            this.ColumnIdEstado.DataSource = loDataSetCbColuna.Tables[0];
        }

        /// <summary>
		/// Metodos de definição dos filtros de busca..
		/// </summary>

        private void DefinirCbFiltroEstado()
        {
            SqlCommand loSqlCommandCbEstado = new SqlCommand(
            "SELECT (est_nm_nome) ds_exibicao, " +
                "(est_id_estado) cd_dado " +
                "FROM EST_estado " +
                "UNION " +
                "SELECT '[TODOS]', " +
                "-1 " +
                "ORDER BY ds_exibicao", loConexao);

            SqlDataAdapter loSqlDataAdapterCbFiltro = new SqlDataAdapter(loSqlCommandCbEstado);
            SqlCommandBuilder loSqlCommandBuilderCbColuna = new SqlCommandBuilder(loSqlDataAdapterCbFiltro);
            DataSet loDataSetCbFiltro = new DataSet();
            loSqlDataAdapterCbFiltro.Fill(loDataSetCbFiltro);
            this.comboBoxEstado.DisplayMember = "ds_exibicao";
            this.comboBoxEstado.ValueMember = "cd_dado";
            this.comboBoxEstado.DataSource = loDataSetCbFiltro.Tables[0];
        }
        /// <summary>
        /// Metodos de redimensionamento da tela e persistência das informações ao adicionar uma nova linha....
        /// <summary>

        private void RedimensionarTela(object sender, EventArgs e)
        {
            if (WindowState != FormWindowState.Minimized)
            {
                Size tempSize = this.Size;
                tempSize.Height = this.dataGridViewCidade.Size.Height + this.Size.Height - ii_heigth_old;
                tempSize.Width = this.dataGridViewCidade.Size.Width + this.Size.Width - ii_width_old;
                ArmazenarAlturaLarguraTela();
                this.dataGridViewCidade.Size = tempSize;
            }
        }

        private void ArmazenarAlturaLarguraTela()
        {
            ii_width_old = this.Size.Width;
            ii_heigth_old = this.Size.Height;
        }

        private void dataGridViewCidade_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            dataGridViewCidade.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

    }
}
