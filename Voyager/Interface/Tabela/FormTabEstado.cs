﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Voyager.Interface.Tabela
{
    public partial class FormTabEstado : Form
    {
        DataSet ioDataSetVoyager;
        SqlDataAdapter ioSqlDataAdapterEstado;
        BindingSource ioBindingSourceEstado;
        int ii_width_old = 0;
        int ii_heigth_old = 0;
        SqlConnection loConexao;

        public FormTabEstado()
        {
            InitializeComponent(); loConexao = new SqlConnection(@"Server=localhost\SQLNexus;Database=Voyager;Trusted_Connection=True;");
            this.ArmazenarAlturaLarguraTela();
            this.dataGridViewEstado.AutoResizeColumns();
            this.dataGridViewEstado.AutoSizeColumnsMode =
                DataGridViewAutoSizeColumnsMode.AllCells;
            ExecutarConsultaEstado();
        }

        /// <summary>
        /// Eventos de clique dos botões do menu...
        /// </summary>

        private void toolStripSaveButton_Click(object sender, EventArgs e)
        {
            dataGridViewEstado.CommitEdit(DataGridViewDataErrorContexts.Commit);
            this.ioBindingSourceEstado.EndEdit();

            try
            {
                if (this.ioSqlDataAdapterEstado.Update((DataTable)this.ioBindingSourceEstado.DataSource) >= 1)
                {
                    MessageBox.Show("Operação executada com sucesso!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.ToString());
            }
        }

        /// <summary>
        /// Metodos de execução no banco de dados, definição de scripts de conexão e parametros...
        /// </summary>

        private void ExecutarConsultaEstado()
        {
            SqlCommand loSqlCommandSelectEstado = ObterSqlCommandEstado();

            this.ioSqlDataAdapterEstado = new SqlDataAdapter(loSqlCommandSelectEstado);
            SqlCommandBuilder loSqlCommandBuilderEstado = new SqlCommandBuilder(this.ioSqlDataAdapterEstado);
            this.ioDataSetVoyager = new DataSet();
            this.ioSqlDataAdapterEstado.Fill(this.ioDataSetVoyager);
            this.ioBindingSourceEstado = new BindingSource();
            this.ioBindingSourceEstado.DataSource = this.ioDataSetVoyager.Tables[0];
            this.bindingNavigatorVoyager.BindingSource = this.ioBindingSourceEstado;
            this.dataGridViewEstado.DataSource = this.ioBindingSourceEstado;
        }

        private SqlCommand ObterSqlCommandEstado()
        {
            SqlCommand loSqlCommandEst = new SqlCommand(
                "SELECT est_id_estado, " +
                "est_nm_nome, " +
                "est_sg_estado " +
                "FROM EST_estado " +
                "ORDER BY est_nm_nome ASC", loConexao);

            return loSqlCommandEst;
        }

        /// <summary>
        /// Metodos de redimensionamento da tela e persistência das informações ao adicionar uma nova linha....
        /// <summary>

        private void RedimensionarTela(object sender, EventArgs e)
        {
            if (WindowState != FormWindowState.Minimized)
            {
                Size tempSize = this.Size;
                tempSize.Height = this.dataGridViewEstado.Size.Height + this.Size.Height - ii_heigth_old;
                tempSize.Width = this.dataGridViewEstado.Size.Width + this.Size.Width - ii_width_old;
                ArmazenarAlturaLarguraTela();
                this.dataGridViewEstado.Size = tempSize;
            }
        }

        private void ArmazenarAlturaLarguraTela()
        {
            ii_width_old = this.Size.Width;
            ii_heigth_old = this.Size.Height;
        }

    }
}
