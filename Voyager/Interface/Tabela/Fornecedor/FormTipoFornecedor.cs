﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Voyager.Interface.Tabela.Fornecedor
{
    public partial class FormTipoFornecedor : Form
    {
        DataSet ioDataSetVoyager;
        SqlDataAdapter ioSqlDataAdapterCidade;
        BindingSource ioBindingSourceCidade;
        int ii_width_old = 0;
        int ii_heigth_old = 0;
        SqlConnection loConexao;

        public FormTipoFornecedor()
        {
            InitializeComponent();
            loConexao = GerenciadorAplicacao.ConexaoBancoDados.DbConnectionStringBuilder;
            this.ArmazenarAlturaLarguraTela();
            this.dataGridViewTipoFornecedor.AutoResizeColumns();
            this.dataGridViewTipoFornecedor.AutoSizeColumnsMode =
                DataGridViewAutoSizeColumnsMode.AllCells;
            ExecutarConsultaTipoFornecedor();
        }

        /// <summary>
        /// Eventos de clique dos botões do menu...
        /// </summary>

        private void toolStripSaveButton_Click(object sender, EventArgs e)
        {
            dataGridViewTipoFornecedor.CommitEdit(DataGridViewDataErrorContexts.Commit);
            this.ioBindingSourceCidade.EndEdit();
            Int32 liContador = 0;
            for (int i = 0; i < dataGridViewTipoFornecedor.RowCount; i++)
            {
                DataGridViewRow gridrow = dataGridViewTipoFornecedor.Rows[i];
                DataRowView rowview = (DataRowView)gridrow.DataBoundItem;
                DataRow row = rowview.Row;

                if (string.IsNullOrEmpty(dataGridViewTipoFornecedor.Rows[i].Cells["ColumnNmTipo"].Value.ToString()))
                {
                    MessageBox.Show("Nenhum nome informado!");
                    return;
                }

                if (row.RowState.Equals(DataRowState.Added))
                {
                    liContador++;
                    dataGridViewTipoFornecedor.Rows[i].Cells["ColumnId"].Value = GerenciadorAplicacao.GerarCodigoChaveTabela("tfn_id_tipo_fornecedor","TFN_tipo_fornecedor") + liContador;
                    dataGridViewTipoFornecedor.Rows[i].Cells["ColumnNmUsuario"].Value = GerenciadorAplicacao.UsuarioAtual.Login;
                    dataGridViewTipoFornecedor.Rows[i].Cells["ColumnDtAtualizacao"].Value = DateTime.Today;
                }
                else if (row.RowState.Equals(DataRowState.Modified))
                {
                    dataGridViewTipoFornecedor.Rows[i].Cells["ColumnNmUsuario"].Value = GerenciadorAplicacao.UsuarioAtual.Login;
                    dataGridViewTipoFornecedor.Rows[i].Cells["ColumnDtAtualizacao"].Value = DateTime.Today;
                }
            }
            try
            {
                if (this.ioSqlDataAdapterCidade.Update((DataTable)this.ioBindingSourceCidade.DataSource) >= 1)
                {
                    MessageBox.Show("Operação executada com sucesso");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.ToString());
            }
            dataGridViewTipoFornecedor.Refresh();
        }

        /// <summary>
        /// Metodos de execução no banco de dados, definição de scripts de conexão e parametros... 
        /// </summary>

        private void ExecutarConsultaTipoFornecedor()
        {
            SqlCommand loSqlCommandSelectTfn = ObterSqlCommandTipoFornecedor();

            this.ioSqlDataAdapterCidade = new SqlDataAdapter(loSqlCommandSelectTfn);
            SqlCommandBuilder loSqlCommadBuilderTfn = new SqlCommandBuilder(this.ioSqlDataAdapterCidade);
            this.ioDataSetVoyager = new DataSet();
            this.ioSqlDataAdapterCidade.Fill(this.ioDataSetVoyager);
            this.ioBindingSourceCidade = new BindingSource();
            this.ioBindingSourceCidade.DataSource = this.ioDataSetVoyager.Tables[0];
            this.bindingNavigatorVoyager.BindingSource = this.ioBindingSourceCidade;
            this.dataGridViewTipoFornecedor.DataSource = this.ioBindingSourceCidade;
        }

        private SqlCommand ObterSqlCommandTipoFornecedor()
        {
            SqlCommand loSqlCommandTipoFornecedor = new SqlCommand(
                "SELECT tfn_id_tipo_fornecedor, " +
                "tfn_nm_tipo, " +
                "tfn_nm_usuario, " +
                "tfn_dt_atualizacao " +
                "FROM TFN_tipo_fornecedor", loConexao);

            return loSqlCommandTipoFornecedor;
        }

        /// <summary>
        /// Metodos de redimensionamento da tela e persistência das informações ao adicionar uma nova linha....
        /// <summary>

        private void RedimensionarTela(object sender, EventArgs e)
        {
            if (WindowState != FormWindowState.Minimized)
            {
                Size tempSize = this.Size;
                tempSize.Height = this.dataGridViewTipoFornecedor.Size.Height + this.Size.Height - ii_heigth_old;
                tempSize.Width = this.dataGridViewTipoFornecedor.Size.Width + this.Size.Width - ii_width_old;
                ArmazenarAlturaLarguraTela();
                this.dataGridViewTipoFornecedor.Size = tempSize;
            }
        }

        private void ArmazenarAlturaLarguraTela()
        {
            ii_width_old = this.Size.Width;
            ii_heigth_old = this.Size.Height;
        }

        private void dataGridViewTipoFornecedor_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            dataGridViewTipoFornecedor.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }
    }
}
