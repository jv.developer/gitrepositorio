﻿namespace Voyager.Interface.Tabela.Fornecedor
{
    partial class FormTipoFornecedor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormTipoFornecedor));
            this.bindingNavigatorVoyager = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.toolStripBuscarButton = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSaveButton = new System.Windows.Forms.ToolStripButton();
            this.dataGridViewTipoFornecedor = new System.Windows.Forms.DataGridView();
            this.ColumnId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnNmTipo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnNmUsuario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDtAtualizacao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigatorVoyager)).BeginInit();
            this.bindingNavigatorVoyager.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTipoFornecedor)).BeginInit();
            this.SuspendLayout();
            // 
            // bindingNavigatorVoyager
            // 
            this.bindingNavigatorVoyager.AddNewItem = this.bindingNavigatorAddNewItem;
            this.bindingNavigatorVoyager.CountItem = this.bindingNavigatorCountItem;
            this.bindingNavigatorVoyager.DeleteItem = this.bindingNavigatorDeleteItem;
            this.bindingNavigatorVoyager.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripBuscarButton,
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.toolStripSaveButton});
            this.bindingNavigatorVoyager.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigatorVoyager.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.bindingNavigatorVoyager.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.bindingNavigatorVoyager.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.bindingNavigatorVoyager.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.bindingNavigatorVoyager.Name = "bindingNavigatorVoyager";
            this.bindingNavigatorVoyager.PositionItem = this.bindingNavigatorPositionItem;
            this.bindingNavigatorVoyager.Size = new System.Drawing.Size(324, 25);
            this.bindingNavigatorVoyager.TabIndex = 3;
            this.bindingNavigatorVoyager.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Adicionar novo";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(37, 22);
            this.bindingNavigatorCountItem.Text = "de {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Número total de itens";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Excluir";
            // 
            // toolStripBuscarButton
            // 
            this.toolStripBuscarButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripBuscarButton.Image = ((System.Drawing.Image)(resources.GetObject("toolStripBuscarButton.Image")));
            this.toolStripBuscarButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripBuscarButton.Name = "toolStripBuscarButton";
            this.toolStripBuscarButton.Size = new System.Drawing.Size(23, 22);
            this.toolStripBuscarButton.Text = "toolStripButton1";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Mover primeiro";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Mover anterior";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Posição";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Posição atual";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Mover próximo";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Mover último";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSaveButton
            // 
            this.toolStripSaveButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripSaveButton.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSaveButton.Image")));
            this.toolStripSaveButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSaveButton.Name = "toolStripSaveButton";
            this.toolStripSaveButton.Size = new System.Drawing.Size(23, 22);
            this.toolStripSaveButton.Text = "toolStripButton1";
            this.toolStripSaveButton.Click += new System.EventHandler(this.toolStripSaveButton_Click);
            // 
            // dataGridViewTipoFornecedor
            // 
            this.dataGridViewTipoFornecedor.AllowUserToAddRows = false;
            this.dataGridViewTipoFornecedor.AllowUserToResizeColumns = false;
            this.dataGridViewTipoFornecedor.AllowUserToResizeRows = false;
            this.dataGridViewTipoFornecedor.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.dataGridViewTipoFornecedor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewTipoFornecedor.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnId,
            this.ColumnNmTipo,
            this.ColumnNmUsuario,
            this.ColumnDtAtualizacao});
            this.dataGridViewTipoFornecedor.Location = new System.Drawing.Point(12, 28);
            this.dataGridViewTipoFornecedor.Name = "dataGridViewTipoFornecedor";
            this.dataGridViewTipoFornecedor.RowHeadersVisible = false;
            this.dataGridViewTipoFornecedor.Size = new System.Drawing.Size(300, 286);
            this.dataGridViewTipoFornecedor.TabIndex = 4;
            this.dataGridViewTipoFornecedor.CurrentCellDirtyStateChanged += new System.EventHandler(this.dataGridViewTipoFornecedor_CurrentCellDirtyStateChanged);
            // 
            // ColumnId
            // 
            this.ColumnId.DataPropertyName = "tfn_id_tipo_fornecedor";
            this.ColumnId.HeaderText = "ColumnId";
            this.ColumnId.Name = "ColumnId";
            this.ColumnId.Visible = false;
            // 
            // ColumnNmTipo
            // 
            this.ColumnNmTipo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ColumnNmTipo.DataPropertyName = "tfn_nm_tipo";
            this.ColumnNmTipo.HeaderText = "Tipo de Fornecedor";
            this.ColumnNmTipo.Name = "ColumnNmTipo";
            this.ColumnNmTipo.Width = 270;
            // 
            // ColumnNmUsuario
            // 
            this.ColumnNmUsuario.DataPropertyName = "tfn_nm_usuario";
            this.ColumnNmUsuario.HeaderText = "ColumnNmUsuario";
            this.ColumnNmUsuario.Name = "ColumnNmUsuario";
            this.ColumnNmUsuario.Visible = false;
            // 
            // ColumnDtAtualizacao
            // 
            this.ColumnDtAtualizacao.DataPropertyName = "tfn_dt_atualizacao";
            this.ColumnDtAtualizacao.HeaderText = "ColumnDtAtualizacao";
            this.ColumnDtAtualizacao.Name = "ColumnDtAtualizacao";
            this.ColumnDtAtualizacao.Visible = false;
            // 
            // FormTipoFornecedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(324, 326);
            this.Controls.Add(this.dataGridViewTipoFornecedor);
            this.Controls.Add(this.bindingNavigatorVoyager);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormTipoFornecedor";
            this.Text = "Tabela - Fornecedor - Tipo de Fornecedor";
            this.Resize += new System.EventHandler(this.RedimensionarTela);
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigatorVoyager)).EndInit();
            this.bindingNavigatorVoyager.ResumeLayout(false);
            this.bindingNavigatorVoyager.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTipoFornecedor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingNavigator bindingNavigatorVoyager;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton toolStripBuscarButton;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripSaveButton;
        private System.Windows.Forms.DataGridView dataGridViewTipoFornecedor;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnNmTipo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnNmUsuario;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDtAtualizacao;
    }
}