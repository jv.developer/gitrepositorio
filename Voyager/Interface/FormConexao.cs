﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Voyager.Interface
{
    public partial class FormConexao : Form
    {
        string asFonteDados;
        string asBancoDados;
        public FormConexao()
        {
            InitializeComponent();
            asFonteDados = @"localhost\SQLNexus";
            asBancoDados = "Voyager";
        }

        private void FormConexao_Load(object sender, EventArgs e)
        {

        }

        private void buttonConectar_Click(object sender, EventArgs e)
        {
            if (!GerenciadorAplicacao.ConexaoBancoDados.Conectar(asFonteDados,asBancoDados,this.textBoxUsuario.Text,this.textBoxSenha.Text))
            {
                MessageBox.Show("Dados de conexão invalidos.", "!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            ((FormPrincipal)this.MdiParent).HabilitarMenu(true);
            this.Close();
        }

        private void buttonFechar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
