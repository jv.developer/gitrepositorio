﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Windows.Forms;
using Voyager.Interface;

namespace Voyager
{
    static class GerenciadorAplicacao
    {
        /// <summary>
        /// Ponto de entrada principal para o aplicativo.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            FormSplashScreen frmSplash = new FormSplashScreen();

            //Abre o formulário de apresentação
            frmSplash.Show();

            //Processa todas as mensagens pendentes do windows.
            //Necessário para carregar a imagem do splash antes de executar o Application.Run
            Application.DoEvents();

            //Carregar configurações do usuário
            for (int liProgresso = 1; liProgresso <= 100; liProgresso++)
            {
                frmSplash.atualizarProgressBar(1);
                Thread.Sleep(20);
            }

            //Fecha formulário de apresentação
            frmSplash.Dispose();

            //Inicia a aplicação com o FrmPrincipal
            Application.Run(new FormPrincipal());
        }

        public static class ConexaoBancoDados
        {
            private static bool ibConectado;
            private static string isUsuario = "joaomiranda";
            private static string isSenha = "miranda";
            private static SqlConnection ioDbConnection;

            public static bool Conectado
            {
                get
                {
                    return ibConectado;
                }
                set
                {
                    ibConectado = value;
                }
            }

            public static string Usuario
            {
                get
                {
                    return isUsuario;
                }
                set
                {
                    isUsuario = value;
                }
            }

            public static string Senha
            {
                get
                {
                    return isSenha;
                }
                set
                {
                    isSenha = value;

                }
            }

            public static SqlConnection DbConnectionStringBuilder
            {
                get { return ioDbConnection; }
                set { ioDbConnection = value; }
            }

            public static bool Conectar(string asFonteDados, string asBancoDados, string asUsuario, string asSenha)
            {
                DbConnectionStringBuilder = new SqlConnection(@"Server="+asFonteDados+";Database="+asBancoDados+";Trusted_Connection=True;");

                //Verificar login de usuario
                SqlDataReader loSqlDataReaderValidacao = null;
                SqlConnection loSqlConnectionValidacao = null;
                try
                {
                    loSqlConnectionValidacao = GerenciadorAplicacao.ConexaoBancoDados.DbConnectionStringBuilder;
                    loSqlConnectionValidacao.Open();
                    SqlCommand loSqlCommandSelectValidacao = new SqlCommand(
                        "SELECT (COUNT(1))Coluna FROM USR_usuario " +
                        "WHERE usr_nm_login = @asNmLogin and usr_ds_senha = @asSenha", loSqlConnectionValidacao);

                    //Define as informações do parametro criado
                    SqlParameter loSqlParameterLoginValidacao = new SqlParameter();
                    loSqlParameterLoginValidacao.ParameterName = "@asNmLogin";
                    loSqlParameterLoginValidacao.Value = asUsuario;
                    SqlParameter loSqlParameterSenhaValidacao = new SqlParameter();
                    loSqlParameterSenhaValidacao.ParameterName = "@asSenha";
                    loSqlParameterSenhaValidacao.Value = asSenha;

                    //Inserindo os parametros no comando
                    loSqlCommandSelectValidacao.Parameters.Add(loSqlParameterLoginValidacao);
                    loSqlCommandSelectValidacao.Parameters.Add(loSqlParameterSenhaValidacao);

                    //Executando o comando e obtendo o resultado
                    loSqlDataReaderValidacao = loSqlCommandSelectValidacao.ExecuteReader();

                    //Exibindo registro
                    loSqlDataReaderValidacao.Read();
                    Int32 liRetornoValidacao = Convert.ToInt32(loSqlDataReaderValidacao["Coluna"]);

                    if (liRetornoValidacao > 0)
                    {
                        Conectado = true;
                    }
                    else
                    {
                        MessageBox.Show("Login ou senha incorretos!");
                        return false;
                    }
                }
                finally 
                {
                    //Fecha o Datareader
                    if (loSqlDataReaderValidacao != null)
                    {
                        loSqlDataReaderValidacao.Close();
                    }
                    //Fecha a conexão
                    if (loSqlConnectionValidacao != null)
                    {
                        loSqlConnectionValidacao.Close();
                    }
                }
                

                GerenciadorAplicacao.UsuarioAtual.Login = asUsuario;
                GerenciadorAplicacao.UsuarioAtual.Senha = asSenha;

                //Recuperar as informações do usuario atual
                SqlDataReader loSqlDataReaderUsr = null;
                SqlConnection loSqlConnectionVoyager = null;
                try
                {
                    loSqlConnectionVoyager = GerenciadorAplicacao.ConexaoBancoDados.DbConnectionStringBuilder;
                    loSqlConnectionVoyager.Open();
                    SqlCommand loSqlCommandSelectUsr = new SqlCommand(
                        "SELECT usr_nm_usuario, usr_id_usuario, usr_tp_usuario, usr_id_empresa " +
                        "FROM USR_usuario WHERE usr_nm_login = @asNmLogin", loSqlConnectionVoyager);

                    //Define as informações do parametro criado
                    SqlParameter loSqlParameterNmLogin = new SqlParameter();
                    loSqlParameterNmLogin.ParameterName = "@asNmLogin";
                    loSqlParameterNmLogin.Value = GerenciadorAplicacao.UsuarioAtual.Login;

                    // Inserindo o parametro no comando
                    loSqlCommandSelectUsr.Parameters.Add(loSqlParameterNmLogin);

                    //Executando o comando e obtendo o resultado
                    loSqlDataReaderUsr = loSqlCommandSelectUsr.ExecuteReader();

                    //Exibindo registros 
                    while (loSqlDataReaderUsr.Read())
                    {
                        GerenciadorAplicacao.UsuarioAtual.Nome = Convert.ToString(loSqlDataReaderUsr["usr_nm_usuario"]);
                        GerenciadorAplicacao.UsuarioAtual.Codigo = Convert.ToInt32(loSqlDataReaderUsr["usr_id_usuario"]);
                        GerenciadorAplicacao.UsuarioAtual.Tipo = Convert.ToString(loSqlDataReaderUsr["usr_tp_usuario"]);
                        GerenciadorAplicacao.UsuarioAtual.Empresa = Convert.ToInt32(loSqlDataReaderUsr["usr_id_empresa"]);
                    }
                }
                finally
                {
                    //Fecha o Datareader
                    if (loSqlDataReaderUsr != null)
                    {
                        loSqlDataReaderUsr.Close();
                    }
                    //Fecha a conexão
                    if (loSqlConnectionVoyager != null)
                    {
                        loSqlConnectionVoyager.Close();
                    }
                }
                
                return true;
            }

            public static void Desconectar()
            {
                ioDbConnection = null;
                Conectado = false;
            }
        }

        public static Int32 GerarCodigoChaveTabela(string asColuna, string asTabela)
        {
            Int32 retornoconsulta = 0;
            SqlDataReader loSqlDataReaderPesquisa = null;
            SqlConnection loSqlConnectionPesquisa = null;
            try
            {
                loSqlConnectionPesquisa = GerenciadorAplicacao.ConexaoBancoDados.DbConnectionStringBuilder;
                loSqlConnectionPesquisa.Open();
                
                SqlCommand loSqlCommandPesquisa = new SqlCommand(
                    "SELECT (ISNULL(max(" + asColuna + "),0)) Coluna " +
                    "FROM " + asTabela, loSqlConnectionPesquisa);

                loSqlDataReaderPesquisa = loSqlCommandPesquisa.ExecuteReader();
                while (loSqlDataReaderPesquisa.Read())
                {
                    retornoconsulta = Convert.ToInt32(loSqlDataReaderPesquisa["Coluna"]);
                }
            }
            finally
            {
                if (loSqlDataReaderPesquisa != null)
                {
                    loSqlDataReaderPesquisa.Close();
                }

                if (loSqlConnectionPesquisa != null)
                {
                    loSqlConnectionPesquisa.Close();
                }
            }
            return retornoconsulta;
        }

        public static class UsuarioAtual
        {
            private static Int32 iiCodigo;
            private static string isTipo;
            private static Int32 iiEmpresa;
            private static string isNome;
            private static string isLogin;
            private static string isSenha;

            public static Int32 Codigo
            {
                get
                {
                    return iiCodigo;
                }
                set
                {
                    iiCodigo = value;
                }
            }

            public static string Tipo
            {
                get
                {
                    return isTipo;
                }
                set
                {
                    isTipo = value;
                }
            }

            public static Int32 Empresa
            {
                get
                {
                    return iiEmpresa;
                }
                set
                {
                    iiEmpresa = value;
                }
            }

            public static string Nome
            {
                get
                {
                    return isNome;
                }
                set
                {
                    isNome = value;
                }
            }

            public static string Login
            {
                get
                {
                    return isLogin;
                }
                set
                {
                    isLogin = value;
                }
            }

            public static string Senha
            {
                get
                {
                    return isSenha;
                }
                set
                {
                    isSenha = value;
                }
            }

        }
    }
}
