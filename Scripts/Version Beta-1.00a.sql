print '-----------------------------------------------------------------------------------------------'
print 'Script SQL da vers�o Beta-1.00a do sistema Voyager'
print '-----------------------------------------------------------------------------------------------'
GO

USE Voyager
GO

PRINT '-----------------------------------------------------------------------------------------------'
PRINT 'XX- Cria��o da tabela EMP_empresa'
PRINT '-----------------------------------------------------------------------------------------------'
GO

IF NOT EXISTS(SELECT 1
		FROM INFORMATION_SCHEMA.TABLES
		WHERE TABLE_SCHEMA = 'dbo'
		AND TABLE_NAME = 'EMP_empresa')
CREATE TABLE EMP_empresa(
	emp_id_empresa NUMERIC(12) NOT NULL IDENTITY,
	emp_nm_razao_social VARCHAR(50) NOT NULL,
	emp_nm_fantasia VARCHAR(30) NOT NULL, 
	emp_nu_cnpj_cpf CHAR(14) NOT NULL,
	emp_dt_atualizacao DATETIME NOT NULL,
	CONSTRAINT PK_ID_EMPRESA PRIMARY KEY(emp_id_empresa)
)

PRINT '-----------------------------------------------------------------------------------------------'
PRINT 'XX- Cria��o da tabela USR_usuario'
PRINT '-----------------------------------------------------------------------------------------------'
GO

IF NOT EXISTS(SELECT 1
		FROM INFORMATION_SCHEMA.TABLES
		WHERE TABLE_SCHEMA = 'dbo'
		AND TABLE_NAME = 'USR_usuario')
CREATE TABLE USR_usuario(
	usr_id_usuario NUMERIC(12) NOT NULL IDENTITY,
	usr_id_empresa NUMERIC(12) NOT NULL,
	usr_nm_usuario VARCHAR(50) NOT NULL,
	usr_nm_login VARCHAR(30) NOT NULL,
	usr_ds_senha VARCHAR(20) NOT NULL,
	usr_tp_usuario CHAR(1) NOT NULL,
	CONSTRAINT PK_ID_USUARIO PRIMARY KEY(usr_id_usuario),
	CONSTRAINT FK_EMP_USR FOREIGN KEY(usr_id_empresa)
	REFERENCES EMP_empresa(emp_id_empresa)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION
)
GO

IF EXISTS (SELECT 1 
	FROM sys.indexes 
	WHERE name='FK_EMP_USR' AND object_id = OBJECT_ID('USR_usuario'))
DROP INDEX USR_usuario.FK_EMP_USR
GO

CREATE INDEX FK_EMP_USR ON USR_usuario(usr_id_empresa)
GO

PRINT '-----------------------------------------------------------------------------------------------'
PRINT 'XX- Cria��o da tabela TFN_tipo_fornecedor'
PRINT '-----------------------------------------------------------------------------------------------'
GO

IF NOT EXISTS(SELECT 1
		FROM INFORMATION_SCHEMA.TABLES
		WHERE TABLE_SCHEMA = 'dbo'
		AND TABLE_NAME = 'TFN_tipo_fornecedor')
CREATE TABLE TFN_tipo_fornecedor(
	tfn_id_tipo_fornecedor NUMERIC(12) NOT NULL IDENTITY,
	tfn_nm_tipo VARCHAR(30) NOT NULL,
	tfn_nm_usuario VARCHAR(30) NOT NULL,
	tfn_dt_atualizacao DATETIME NOT NULL,
	CONSTRAINT PK_ID_TIPO_FORNECEDOR PRIMARY KEY(tfn_id_tipo_fornecedor)
)

GO

PRINT '-----------------------------------------------------------------------------------------------'
PRINT 'XX- Cria��o da tabela FRN_fornecedor'
PRINT '-----------------------------------------------------------------------------------------------'
GO

IF NOT EXISTS(SELECT 1
		FROM INFORMATION_SCHEMA.TABLES
		WHERE TABLE_SCHEMA = 'dbo'
		AND TABLE_NAME = 'FRN_fornecedor')
CREATE TABLE FRN_fornecedor(
	frn_id_fornecedor numeric(12) not null identity,
	frn_nm_fornecedor varchar(50) not null,
	frn_ds_endereco varchar(50) null,
	frn_id_tipo_fornecedor NUMERIC(12) not null,
	frn_nu_telefone VARCHAR(11) NULL,
	frn_ds_email VARCHAR(100) NULL,
	frn_nm_responsavel VARCHAR(30) NULL,
	frn_ds_bancaria VARCHAR(50) NULL,
	frn_nm_usuario VARCHAR(30) NOT NULL,
	frn_dt_atualizacao DATETIME NOT NULL,
	CONSTRAINT PK_ID_FORNECEDOR PRIMARY KEY(frn_id_fornecedor),
	CONSTRAINT FK_TFN_FRN FOREIGN KEY(frn_id_tipo_fornecedor)
	REFERENCES TFN_tipo_fornecedor(tfn_id_tipo_fornecedor)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION
)

GO

IF EXISTS (SELECT 1 
	FROM sys.indexes 
	WHERE name='FK_TFN_FRN' AND object_id = OBJECT_ID('FRN_fornecedor'))
DROP INDEX FRN_fornecedor.FK_TFN_FRN
GO

CREATE INDEX FK_TFN_FRN ON FRN_fornecedor(frn_id_tipo_fornecedor)
GO

PRINT '-----------------------------------------------------------------------------------------------'
PRINT 'XX- Cria��o da tabela CLT_cliente'
PRINT '-----------------------------------------------------------------------------------------------'
GO
 
IF NOT EXISTS(SELECT 1
		FROM INFORMATION_SCHEMA.TABLES
		WHERE TABLE_SCHEMA = 'dbo'
		AND TABLE_NAME = 'CLT_cliente')
CREATE TABLE CLT_cliente(
	clt_id_cliente numeric(12) not null identity,
	clt_nm_cliente varchar(30) not null,
	clt_nu_telefone numeric(14) null,
	clt_nu_rg varchar(14) null,
	clt_nu_cpf varchar(14) null,
	clt_ds_endereco varchar(50) null,
	clt_dt_nascimento datetime null,
	clt_id_empresa numeric(12) not null,
	clt_nm_usuario VARCHAR(30) NOT NULL,
	clt_dt_atualizacao DATETIME NOT NULL,
	constraint PK_ID_CLIENTE primary key(clt_id_cliente),
	CONSTRAINT FK_EMP_CLT FOREIGN KEY (clt_id_empresa)
	REFERENCES EMP_empresa(emp_id_empresa)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION
)
go

IF EXISTS (SELECT 1 
	FROM sys.indexes 
	WHERE name='FK_EMP_CLT' AND object_id = OBJECT_ID('CLT_cliente'))
DROP INDEX CLT_cliente.FK_EMP_CLT
GO

CREATE INDEX FK_EMP_CLT ON CLT_cliente(clt_id_empresa)
GO

PRINT '-----------------------------------------------------------------------------------------------'
PRINT 'XX- Cria��o da tabela EST_estado'
PRINT '-----------------------------------------------------------------------------------------------'
GO

IF NOT EXISTS(SELECT 1
		FROM INFORMATION_SCHEMA.TABLES
		WHERE TABLE_SCHEMA = 'dbo'
		AND TABLE_NAME = 'EST_estado')
CREATE TABLE EST_estado(
	est_id_estado numeric(12) not null identity,
	est_nm_estado varchar(25) null,
	est_sg_estado char(2) null,
	constraint PK_ID_ESTADO primary key(est_id_estado)
	)
go

PRINT '-----------------------------------------------------------------------------------------------'
PRINT 'XX- Cria��o da tabela CID_cidade'
PRINT '-----------------------------------------------------------------------------------------------'
GO

IF NOT EXISTS(SELECT 1
		FROM INFORMATION_SCHEMA.TABLES
		WHERE TABLE_SCHEMA = 'dbo'
		AND TABLE_NAME = 'CID_cidade')
CREATE TABLE CID_cidade(
	cid_id_cidade numeric(12) not null,
	cid_nm_cidade varchar(30) NOT NULL,
	cid_id_estado numeric(12) NOT NULL,
	cid_cd_cidade_ibge NUMERIC(3) NULL,
	CONSTRAINT PK_ID_CIDADE PRIMARY KEY(cid_id_cidade),
	CONSTRAINT FK_EST_CID FOREIGN KEY(cid_id_estado)
	REFERENCES EST_estado(est_id_estado)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION
	)
go

IF EXISTS (SELECT 1 
	FROM sys.indexes 
	WHERE name='FK_EST_CID' AND object_id = OBJECT_ID('CID_cidade'))
DROP INDEX CID_cidade.FK_EST_CID
GO

CREATE INDEX FK_EST_CID ON CID_cidade(cid_id_estado)
GO

PRINT '-----------------------------------------------------------------------------------------------'
PRINT 'XX- Cria��o da tabela VIA_viagem'
PRINT '-----------------------------------------------------------------------------------------------'
GO

IF NOT EXISTS(SELECT 1
		FROM INFORMATION_SCHEMA.TABLES
		WHERE TABLE_SCHEMA = 'dbo'
		AND TABLE_NAME = 'VIA_viagem')
CREATE TABLE VIA_viagem(
	via_id_viagem numeric(12) not null,
	via_id_fornecedor numeric(12) not null,
	via_id_cidade numeric(12) null,
	via_dt_ida date null,
	via_dt_volta date null,
	via_vl_unitario numeric(6) null,
	via_id_empresa numeric(12) not null,
	via_nm_usuario VARCHAR(30) NOT NULL,
	via_dt_atualizacao DATETIME NOT NULL,
	CONSTRAINT PK_ID_VIAGEM PRIMARY KEY(via_id_viagem),
	CONSTRAINT FK_FRN_VIA FOREIGN KEY(via_id_fornecedor)
	REFERENCES FRN_fornecedor(frn_id_fornecedor)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION,
	CONSTRAINT FK_CID_VIA FOREIGN KEY(via_id_cidade)
	REFERENCES CID_cidade(cid_id_cidade)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION,
	CONSTRAINT FK_EMP_VIA FOREIGN KEY(via_id_empresa)
	REFERENCES EMP_empresa(emp_id_empresa)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION
	)
go

IF EXISTS (SELECT 1 
	FROM sys.indexes 
	WHERE name='FK_FRN_VIA' AND object_id = OBJECT_ID('VIA_viagem'))
DROP INDEX VIA_viagem.FK_FRN_VIA
GO

CREATE INDEX FK_FRN_VIA ON VIA_viagem(via_id_fornecedor)
GO

IF EXISTS (SELECT 1
	FROM sys.indexes
	WHERE name='FK_CID_VIA' AND object_id = OBJECT_ID('VIA_viagem'))
DROP INDEX VIA_viagem.FK_CID_VIA
GO

CREATE INDEX FK_CID_VIA ON VIA_viagem(via_id_cidade)
GO

IF EXISTS (SELECT 1
	FROM sys.indexes
	WHERE name='FK_EMP_VIA' AND object_id = OBJECT_ID('VIA_viagem'))
DROP INDEX VIA_viagem.FK_EMP_VIA
GO

CREATE INDEX FK_EMP_VIA ON VIA_viagem(via_id_empresa)
GO

PRINT '-----------------------------------------------------------------------------------------------'
PRINT 'XX- Cria��o da tabela CLV_cliente_viagem'
PRINT '-----------------------------------------------------------------------------------------------'
GO

IF NOT EXISTS(SELECT 1
		FROM INFORMATION_SCHEMA.TABLES
		WHERE TABLE_SCHEMA = 'dbo'
		AND TABLE_NAME = 'CLV_cliente_viagem')
CREATE TABLE CLV_cliente_viagem(
	clv_id_cliente_viagem NUMERIC(12) NOT NULL IDENTITY,
	clv_id_viagem NUMERIC(12) NOT NULL,
	clv_id_cliente NUMERIC(12) NOT NULL,
	clv_nm_usuario VARCHAR(30) NOT NULL,
	clv_dt_atualizacao DATETIME NOT NULL,
	CONSTRAINT PK_ID_CLIENTE_VIAGEM PRIMARY KEY(clv_id_cliente_viagem),
	CONSTRAINT FK_VIA_CLV FOREIGN KEY(clv_id_viagem)
	REFERENCES VIA_viagem(via_id_viagem)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION,
	CONSTRAINT FK_CLT_CLV FOREIGN KEY(clv_id_cliente)
	REFERENCES CLT_cliente(clt_id_cliente)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION
)
GO

IF EXISTS (SELECT 1 
	FROM sys.indexes 
	WHERE name='FK_VIA_CLV' AND object_id = OBJECT_ID('CLV_cliente_viagem'))
DROP INDEX CLV_cliente_viagem.FK_VIA_CLV
GO

CREATE INDEX FK_VIA_CLV ON CLV_cliente_viagem(clv_id_viagem)
GO

IF EXISTS (SELECT 1 
	FROM sys.indexes 
	WHERE name='FK_CLT_CLV' AND object_id = OBJECT_ID('CLV_cliente_viagem'))
DROP INDEX CLV_cliente_viagem.FK_CLT_CLV
GO

CREATE INDEX FK_CLT_CLV ON CLV_cliente_viagem(clv_id_cliente)
GO

PRINT '-----------------------------------------------------------------------------------------------'
PRINT 'XX- Cria��o da tabela ICV_informacao_cliente_viagem'
PRINT '-----------------------------------------------------------------------------------------------'
GO

IF NOT EXISTS(SELECT 1
		FROM INFORMATION_SCHEMA.TABLES
		WHERE TABLE_SCHEMA = 'dbo'
		AND TABLE_NAME = 'ICV_informacao_cliente_viagem')
CREATE TABLE ICV_informacao_cliente_viagem(
	icv_id_informacao_cliente_viagem NUMERIC(12) NOT NULL IDENTITY,
	icv_id_cliente_viagem NUMERIC(12) NOT NULL,
	icv_vl_pagamento NUMERIC(6) NULL,
	icv_ds_forma_pagamento CHAR(2) NULL,
	icv_vl_parcelas NUMERIC(2) NULL,
	icv_ds_situacao_pagamento CHAR(2) NULL,
	CONSTRAINT PK_ID_INFO_CLIENTE_VIAGEM PRIMARY KEY(icv_id_informacao_cliente_viagem),
	CONSTRAINT FK_CLV_ICV FOREIGN KEY(icv_id_cliente_viagem)
	REFERENCES CLV_cliente_viagem(clv_id_cliente_viagem)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION
)
GO

IF EXISTS (SELECT 1 
	FROM sys.indexes 
	WHERE name='FK_CLV_ICV' AND object_id = OBJECT_ID('ICV_informacao_cliente_viagem'))
DROP INDEX ICV_informacao_cliente_viagem.FK_CLV_ICV
GO

CREATE INDEX FK_CLV_ICV ON ICV_informacao_cliente_viagem(icv_id_cliente_viagem)
GO